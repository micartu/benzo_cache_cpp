//
// Created by Michael Artuerhof, 2019
//
#include <dobject.hpp>
#include <dbtext.hpp>
#include <dbvalue.hpp>
#include <dbquery.hpp>
#include <common_fun.hpp>
#include <memory>
#include "blcacheimp.hpp"
#include "user.hpp"
#include "transaction.hpp"
#include "station.hpp"
#include "map_patch.hpp"
#include "map_patch_stations.hpp"
#include "web_page.hpp"
#include "gtest/gtest.h"

using namespace benzo_cache;

using dbinteger = dbvalue<int>;
using dbdouble = dbvalue<double>;

static const char* t = "test";

class BlCacheTests : public ::testing::Test {

protected:
  virtual void SetUp() {
	  _cache = Blcache::create_with_path("dbfile.db");
	  auto l = UserLoyality(t, 0, t, t, t, t, t, t);
	  _usr = std::make_shared<User>(t, t, t, t, t, t, t, t, 0, t, t, t, 1, 14, l, true, t);
  };

  virtual void TearDown() {
  };

  std::shared_ptr<Blcache> _cache;
  std::shared_ptr<User> _usr;
};


TEST_F(BlCacheTests, CreateAndFetchUserTest)
{
	const char* n = "name";
	auto u2 = User(t, n, n, n, n, n, n, n, 5, n, n, n, 1, 1, _usr->loyality, false, n);
	bool e = _cache->exists_user(t);
	EXPECT_EQ(e, false);
	_cache->create_user(*_usr);
	e = _cache->exists_user(t);
	EXPECT_EQ(e, true);
	_cache->update_user(u2);
	auto u = _cache->get_user(t);
	ASSERT_NE(u, std::nullopt);
	EXPECT_EQ((*u).name, u2.name);
	EXPECT_EQ((*u).surname, u2.surname);
	_cache->delete_user(t);
	e = _cache->exists_user(t);
	EXPECT_EQ(e, false);
}

TEST_F(BlCacheTests, TransactionsTest)
{
	const char* rrn = "rrn_test";
	_cache->create_user(*_usr);
	auto tr = Transaction(rrn,
			std::chrono::system_clock::from_time_t(111),
			"type",
			"brand",
			"location",
			"benzin",
			1.0,
			10,
			11,
			12,
			"none",
			"no_comment",
			std::chrono::system_clock::from_time_t(555),
			13,
			14,
			15,
			"channel",
			"innertype");
	auto tc = tr;
	tc.innertype = "changed_type";
	tc.discount_sign = 25;
	auto e = _cache->exists_transaction(t, rrn);
	EXPECT_EQ(e, false);
	_cache->create_transaction(t, tr);
	auto tt = _cache->get_transaction(t, rrn);
	ASSERT_NE(tt, std::nullopt);
	EXPECT_EQ((*tt).rrn, tr.rrn);
	EXPECT_EQ((*tt).creation_date, tr.creation_date);
	_cache->update_transaction(tc);
	tt = _cache->get_transaction(t, rrn);
	ASSERT_NE(tt, std::nullopt);
	EXPECT_EQ((*tt).innertype, tc.innertype);
	_cache->delete_transaction(rrn);
	e = _cache->exists_transaction(t, rrn);
	EXPECT_EQ(e, false);
	_cache->delete_user(t);
	e = _cache->exists_user(t);
	EXPECT_EQ(e, false);
}

TEST_F(BlCacheTests, WebPageTest)
{
	const char* wname = "webpage";
	const auto w = WebPage(wname,
			std::chrono::system_clock::from_time_t(111),
			wname);
	auto p = w;
	p.content = "changed_content";
	_cache->create_user(*_usr);
	auto e = _cache->exists_webpage(t, wname);
	EXPECT_EQ(e, false);
	_cache->create_webpage(t, w);

	e = _cache->exists_webpage(t, wname);
	EXPECT_EQ(e, true);
	auto ww = _cache->get_webpage(t, wname);
	ASSERT_NE(ww, std::nullopt);
	EXPECT_EQ((*ww).content, w.content);
	_cache->update_webpage(t, p);
	ww = _cache->get_webpage(t, wname);
	ASSERT_NE(ww, std::nullopt);
	EXPECT_EQ((*ww).content, p.content);
	_cache->delete_webpage(t, wname);
	_cache->delete_user(t);
	e = _cache->exists_user(t);
	EXPECT_EQ(e, false);
}

TEST_F(BlCacheTests, PatchTest)
{
	const int32_t id180_90 = 41340707;
	const auto p = MapPatch(id180_90,
			std::chrono::system_clock::from_time_t(555));
	auto p2 = p;
	p2.creation_date = std::chrono::system_clock::from_time_t(111);
	auto e = _cache->exists_patch(id180_90);
	EXPECT_EQ(e, false);
	_cache->create_patch(p);
	e = _cache->exists_patch(id180_90);
	EXPECT_EQ(e, true);
	_cache->update_patch(p2);
	auto pp = _cache->get_patch(id180_90);
	ASSERT_NE(pp, std::nullopt);
	EXPECT_EQ((*pp).creation_date, p2.creation_date);
	_cache->delete_patch(id180_90);
	e = _cache->exists_patch(id180_90);
	EXPECT_EQ(e, false);
}

const Station gen_station(const std::string name);

TEST_F(BlCacheTests, AppendStationsTest)
{
	std::vector<Station> stations;
	for (int i = 0; i < 10; i++) {
		std::ostringstream qs;
		qs << "station_" << i;
		auto s = gen_station(qs.str());
		stations.push_back(std::move(s));
	}
	const auto pid = get_patch_id(0, 0);
	const auto p = MapPatch(pid,
			std::chrono::system_clock::from_time_t(555));
	const auto ps = MapPatchStations(pid,
			stations);
	_cache->create_patch(p);
	_cache->create_stations(stations);
	auto pp =_cache->get_patch_with_stations(pid);
	ASSERT_NE(pp, std::nullopt);
	EXPECT_EQ((*pp).stations.size(), stations.size());
	_cache->delete_patch(pid);
	pp = _cache->get_patch_with_stations(pid);
	ASSERT_EQ(pp, std::nullopt);
}
