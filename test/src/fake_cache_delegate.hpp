#pragma once

#include "bcache_listener.hpp"
#include "common_types.hpp"
#include "user.hpp"
#include "transaction.hpp"
#include "web_page.hpp"
#include "map_patch.hpp"
#include "map_patch_stations.hpp"
#include "gmock/gmock.h"

class FakeCacheDelegate : public BcacheListener {
	public:
		FakeCacheDelegate() = default;

		MOCK_METHOD2(user_exists_completed, void (seqnum id, bool exists));
		MOCK_METHOD2(transaction_exists_completed, void (seqnum id, bool exists));
		MOCK_METHOD2(webpage_exists_completed, void (seqnum id, bool exists));
		MOCK_METHOD2(patch_exists_completed, void (seqnum id, bool exists));

		MOCK_METHOD1(create_user_completed, void (seqnum id));
		MOCK_METHOD1(create_transaction_completed, void (seqnum id));
		MOCK_METHOD1(create_webpage_completed, void (seqnum id));
		MOCK_METHOD1(create_patch_completed, void (seqnum id));
		MOCK_METHOD1(create_stations_completed, void (seqnum id));

		MOCK_METHOD2(get_user_completed, void (seqnum id, const User& u));
		MOCK_METHOD1(get_user_unk_completed, void (seqnum id));
		MOCK_METHOD2(get_transaction_completed, void (seqnum id, const Transaction& t));
		MOCK_METHOD1(get_transaction_unk_completed, void (seqnum id));
		MOCK_METHOD2(get_webpage_completed, void (seqnum id, const WebPage& t));
		MOCK_METHOD1(get_webpage_unk_completed, void (seqnum id));
		MOCK_METHOD2(get_patch_completed, void (seqnum id, const MapPatch& p));
		MOCK_METHOD1(get_patch_unk_completed, void (seqnum id));
		MOCK_METHOD2(get_patch_stations_completed, void (seqnum id, const MapPatchStations& p));

		MOCK_METHOD1(update_user_completed, void (seqnum id));
		MOCK_METHOD1(update_transaction_completed, void (seqnum id));
		MOCK_METHOD1(update_webpage_completed, void (seqnum id));
		MOCK_METHOD1(update_patch_completed, void (seqnum id));

		MOCK_METHOD1(delete_user_completed, void (seqnum id));
		MOCK_METHOD1(delete_transaction_completed, void (seqnum id));
		MOCK_METHOD1(delete_webpage_completed, void (seqnum id));
		MOCK_METHOD1(delete_patch_completed, void (seqnum id));
};
