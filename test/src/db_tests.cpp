//
// Created by Michael Artuerhof, 2019
//
#include <dobject.hpp>
#include <dbtext.hpp>
#include <dbvalue.hpp>
#include <dbquery.hpp>
#include <sqlite3pp.h>
#include <memory>
#include "gtest/gtest.h"

using namespace std;
using namespace benzo_cache;
using dbinteger = dbvalue<int>;
using dbdouble = dbvalue<double>;
//using dbtext = dbvalue<std::string>;

class DBTests : public ::testing::Test {

protected:
  virtual void SetUp() {
	  _db = std::make_unique<sqlite3pp::database>("dbfile.db");
	  const char* initq =
		  "create table if not exists user ("
		  "   _id integer primary key autoincrement not null,"
		  "   age int,"
		  "   name text,"
		  "   weight real"
		  ");";
	  _db->execute(initq);
  };

  virtual void TearDown() {
	  _db->execute("drop table if exists user;");
  };

  std::unique_ptr<sqlite3pp::database> _db;
};

TEST_F(DBTests, SimpleQueryTest)
{
	benzo_cache::dbquery q(*_db,
			"insert into user (age,name,weight) values (?,?,?);",
			{dbinteger(20), dbtext("Alice"), dbdouble(83.25)});
	q.execute();
	std::string str_count;
	sqlite3pp::query qry(*_db, "select count(*) from user");
	sqlite3pp::query::iterator i = qry.begin();
	(*i).getter() >> str_count;
	qry.finish();
	int count = std::stoi(str_count);
	EXPECT_EQ(count, 1);
}

TEST_F(DBTests, TypeTest)
{
	auto di = dbinteger(20);
	EXPECT_STREQ(di.data_class(), "i");
	auto dd = dbdouble(20);
	EXPECT_STREQ(dd.data_class(), "d");
	auto df = dbvalue<float>(10.12f);
	EXPECT_STREQ(df.data_class(), "f");
}

TEST_F(DBTests, FetchQueryTest)
{
	const char* n1 = "bob";
	const char* n2 = "alice";
	const double weight = 99.9f;
	const int age = 32;
	for (int i = 0; i < 2; i++) {
		auto n = (i == 0) ? n1 : n2;
		benzo_cache::dbquery iq(*_db,
				"insert into user (age,name,weight) values (?,?,?);",
				{
				dbinteger(age),
				dbtext(n),
				dbdouble(weight)
				});
		iq.execute();
	}
	// fetch-query:
	benzo_cache::dbquery fq(*_db,
			"select age,name,weight from user;",
			{dbinteger(), dbtext(), dbdouble()}, true);
	fq.execute();
	auto users = fq.result();
	int i = 0;
	EXPECT_EQ(users.size(), 2);
	for (auto v : users) {
		EXPECT_EQ(v.size(), 3);
		dbinteger a = v[0];
		EXPECT_EQ(a.get_val(), age);
		dbtext name = v[1];
		auto n = (i == 0) ? n1 : n2;
		EXPECT_STREQ(name.get_text().c_str(), n);
		dbdouble w = v[2];
		EXPECT_NEAR(w.get_val(), weight, 0.1);
		++i;
	}
}

TEST_F(DBTests, MultipleQueryTest)
{
	// same test as above, but packed in one query object:
	const char* n1 = "bob";
	const char* n2 = "alice";
	const double weight = 99.9f;
	const char* insquery = "insert into user (age,name,weight) values (?,?,?);";
	const int age = 32;
	benzo_cache::dbquery q(*_db,
			{insquery,
			insquery,
			"select age,name,weight from user;"},
			{false, // no fetch
			false, // no fetch
			true}, // fetch query
			{
				{
				dbinteger(age),
				dbtext(n1),
				dbdouble(weight)
				},
				{
				dbinteger(age),
				dbtext(n2),
				dbdouble(weight)
				},
				{
				dbinteger(),
				dbtext(),
				dbdouble()
				}
			});
	q.execute();
	auto res = q.results();
	ASSERT_EQ(res.size(), 3);
	EXPECT_EQ(res[0].size(), 0);
	EXPECT_EQ(res[1].size(), 0);
	auto users = res[2];
	int i = 0;
	for (auto v : users) {
		ASSERT_EQ(v.size(), 3);
		dbinteger a = v[0];
		EXPECT_EQ(a.get_val(), age);
		dbtext name = v[1];
		auto n = (i == 0) ? n1 : n2;
		EXPECT_STREQ(name.get_text().c_str(), n);
		dbdouble w = v[2];
		EXPECT_NEAR(w.get_val(), weight, 0.1);
		++i;
	}
}

TEST_F(DBTests, MultipleQueryWithCodewordsTest)
{
	const char* n1 = "bob";
	const double weight = 89.9f;
	const char* insquery = "insert into user (age,name,weight) values (?,?,?);";
	const int age = 32;
	benzo_cache::dbquery q(*_db,
			{"insert into user (age,name,weight) values (?,?,?);",
			"select age, name from user;",
			"insert into user (age,name,weight) values (%row_id(0),'%s(1,1)',?);",
			"select age, name from user;"},
			{false, // no fetch
			true, // fetch
			false, // no fetch
			true}, // fetch
			{
				{
				dbinteger(age),
				dbtext(n1),
				dbdouble(weight)
				},
				{
				dbtext(),
				dbtext()
				},
				{
				dbdouble(weight)
				},
				{
				dbinteger(),
				dbtext()
				},
			});
	q.execute();
	auto res = q.results();
	ASSERT_EQ(res.size(), 4);
	EXPECT_EQ(res[0].size(), 1);
	EXPECT_EQ(res[1].size(), 1);
	EXPECT_EQ(res[2].size(), 0);
	auto users = res[3];
	int i = 0;
	for (auto v : users) {
		ASSERT_EQ(v.size(), 2);
		dbinteger a = v[0];
		int an = (i == 0) ? age : 1;
		EXPECT_EQ(a.get_val(), an);
		dbtext name = v[1];
		EXPECT_STREQ(name.get_text().c_str(), n1);
		++i;
	}
}
