//
// Created by Michael Artuerhof, 2019
//
#include <dobject.hpp>
#include <dbtext.hpp>
#include <dbvalue.hpp>
#include <dbquery.hpp>
#include <common_fun.hpp>
#include <memory>
#include "bcacheimp.hpp"
#include "user.hpp"
#include "transaction.hpp"
#include "station.hpp"
#include "map_patch_stations.hpp"
#include "web_page.hpp"
#include "gtest/gtest.h"
#include "fake_cache_delegate.hpp"

using namespace benzo_cache;

// google testing friends
using ::testing::Field;
using ::testing::_;

using dbinteger = dbvalue<int>;
using dbdouble = dbvalue<double>;

static const char* t = "test";

class BCacheTests : public ::testing::Test {

protected:
  virtual void SetUp() {
	  _delegate = std::make_shared<FakeCacheDelegate>();
	  _cache = Bcache::create_with_path("dbfile.db", _delegate);
	  auto l = UserLoyality(t, 0, t, t, t, t, t, t);
	  _usr = std::make_shared<User>(t, t, t, t, t, t, t, t, 0, t, t, t, 1, 14, l, true, t);
  };

  virtual void TearDown() {
  };

  void create_user() {
	  _cache->create_user(*_usr);
  }

  std::shared_ptr<FakeCacheDelegate> _delegate;
  std::shared_ptr<Bcache> _cache;
  std::shared_ptr<User> _usr;
};


MATCHER_P(checkUserEq, u,"") {
	return (arg.login == u.login) &&
		(arg.name == u.name) &&
		(arg.balance == u.balance) &&
		(arg.menu_refresh_date == u.menu_refresh_date) &&
		(arg.codeword == u.codeword) &&
		(arg.pin == u.pin) &&
		(arg.card_blocked == u.card_blocked);
}

TEST_F(BCacheTests, CreateAndFetchUserTest)
{
	/*
	UserLoyality(std::string cpa_crd_code_,
                 int32_t cpa_crd_rub_,
                 std::string cpa_prt_code_,
                 std::string cpa_prt_name_,
                 std::string cpa_prt_ref1_,
                 std::string cpa_prt_ref2_,
                 std::string cpa_prt_ref3_,
                 std::string cpa_prt_subid_);
	User(std::string login_,
         std::string name_,
         std::string phone_,
         std::string surname_,
         std::string midname_,
         std::string avatar_,
         std::string pin_,
         std::string card_,
         int64_t birthday_,
         std::string email_,
         std::string codeword_,
         std::string fav_fuel_,
         float balance_,
         int64_t menu_refresh_date_,
         UserLoyality loyality_,
         bool card_blocked_,
         std::string apn_);
	*/
	const char* n = "name";
	auto u2 = User(t, n, n, n, n, n, n, n, 5, n, n, n, 1, 1, _usr->loyality, false, n);
	create_user();
	_cache->exists_user(t);
	_cache->get_user(t);
	_cache->update_user(u2);
	_cache->get_user(t);
	_cache->delete_user(t);
	_cache->exists_user(t);
	_cache->get_user(t);
	EXPECT_CALL(*_delegate, create_user_completed(0))
		.Times(1);
	EXPECT_CALL(*_delegate, user_exists_completed(1, true))
		.Times(1);
	EXPECT_CALL(*_delegate, get_user_completed(2, checkUserEq(*_usr)))
		.Times(1);
	EXPECT_CALL(*_delegate, update_user_completed(3))
		.Times(1);
	EXPECT_CALL(*_delegate, get_user_completed(4, checkUserEq(u2)))
		.Times(1);
	EXPECT_CALL(*_delegate, delete_user_completed(5))
		.Times(1);
	EXPECT_CALL(*_delegate, user_exists_completed(6, false))
		.Times(1);
	EXPECT_CALL(*_delegate, get_user_unk_completed(7))
		.Times(1);
}

MATCHER_P(checkTransEq, t, "") {
	return (arg.rrn == t.rrn) &&
		(arg.creation_date == t.creation_date) &&
		(arg.trn_type == t.trn_type) &&
		(arg.product_quantity == t.product_quantity) &&
		(arg.discount_sign == t.discount_sign) &&
		(arg.discount_value == t.discount_value) &&
		(arg.convert_bonus_date == t.convert_bonus_date) &&
		(arg.innertype == t.innertype);
}

TEST_F(BCacheTests, TransactionsTest)
{
	const char* rrn = "rrn_test";
	create_user();
	auto tr = Transaction(rrn,
			std::chrono::system_clock::from_time_t(111),
			"type",
			"brand",
			"location",
			"benzin",
			1.0,
			10,
			11,
			12,
			"none",
			"no_comment",
			std::chrono::system_clock::from_time_t(555),
			13,
			14,
			15,
			"channel",
			"innertype");
	auto tc = tr;
	tc.innertype = "changed_type";
	tc.discount_sign = 25;
	_cache->exists_user(t);
	_cache->exists_transaction(t, rrn);
	_cache->create_transaction(t, tr);
	_cache->exists_transaction(t, rrn);
	_cache->get_transaction(t, rrn);
	_cache->update_transaction(tc);
	_cache->get_transaction(t, rrn);
	_cache->delete_transaction(rrn);
	_cache->exists_transaction(t, rrn);
	_cache->get_transaction(t, rrn);
	_cache->delete_user(t);
	EXPECT_CALL(*_delegate, create_user_completed(0))
		.Times(1);
	EXPECT_CALL(*_delegate, user_exists_completed(1, true))
		.Times(1);
	EXPECT_CALL(*_delegate, transaction_exists_completed(2, false))
		.Times(1);
	EXPECT_CALL(*_delegate, create_transaction_completed(3))
		.Times(1);
	EXPECT_CALL(*_delegate, transaction_exists_completed(4, true))
		.Times(1);
	EXPECT_CALL(*_delegate, get_transaction_completed(5, checkTransEq(tr)))
		.Times(1);
	EXPECT_CALL(*_delegate, update_transaction_completed(6))
		.Times(1);
	EXPECT_CALL(*_delegate, get_transaction_completed(7, checkTransEq(tc)))
		.Times(1);
	EXPECT_CALL(*_delegate, delete_transaction_completed(8))
		.Times(1);
	EXPECT_CALL(*_delegate, transaction_exists_completed(9, false))
		.Times(1);
	EXPECT_CALL(*_delegate, get_transaction_unk_completed(10))
		.Times(1);
	EXPECT_CALL(*_delegate, delete_user_completed(11))
		.Times(1);
}

MATCHER_P(checkWebPageEq, t, "") {
	return (arg.name == t.name) &&
		(arg.cdate == t.cdate) &&
		(arg.content == t.content);
}

TEST_F(BCacheTests, WebPageTest)
{
	const char* wname = "webpage";
	const auto w = WebPage(wname,
			std::chrono::system_clock::from_time_t(111),
			wname);
	auto p = w;
	p.content = "changed_content";
	create_user();
	_cache->exists_webpage(t, wname);
	_cache->create_webpage(t, w);
	_cache->exists_webpage(t, wname);
	_cache->get_webpage(t, wname);
	_cache->update_webpage(t, p);
	_cache->get_webpage(t, wname);
	_cache->delete_webpage(t, wname);
	_cache->get_webpage(t, wname);
	_cache->delete_user(t);
	EXPECT_CALL(*_delegate, create_user_completed(0))
		.Times(1);
	EXPECT_CALL(*_delegate, webpage_exists_completed(1, false))
		.Times(1);
	EXPECT_CALL(*_delegate, create_webpage_completed(2))
		.Times(1);
	EXPECT_CALL(*_delegate, webpage_exists_completed(3, true))
		.Times(1);
	EXPECT_CALL(*_delegate, get_webpage_completed(4, checkWebPageEq(w)))
		.Times(1);
	EXPECT_CALL(*_delegate, update_webpage_completed(5))
		.Times(1);
	EXPECT_CALL(*_delegate, get_webpage_completed(6, checkWebPageEq(p)))
		.Times(1);
	EXPECT_CALL(*_delegate, delete_webpage_completed(7))
		.Times(1);
	EXPECT_CALL(*_delegate, get_webpage_unk_completed(8))
		.Times(1);
	EXPECT_CALL(*_delegate, delete_user_completed(9))
		.Times(1);
}

TEST_F(BCacheTests, PatchTest)
{
	const int32_t id180_90 = 41340707;
	const auto p = MapPatch(id180_90,
			std::chrono::system_clock::from_time_t(555));
	auto p2 = p;
	p2.creation_date = std::chrono::system_clock::from_time_t(111);
	EXPECT_EQ(id180_90, get_patch_id(180, 90));
	EXPECT_EQ(41340704, get_patch_id(-180, -90));
	_cache->exists_patch(id180_90);
	_cache->create_patch(p);
	_cache->exists_patch(id180_90);
	_cache->update_patch(p2);
	_cache->get_patch(id180_90);
	_cache->delete_patch(id180_90);
	_cache->exists_patch(id180_90);
	EXPECT_CALL(*_delegate, patch_exists_completed(0, false))
		.Times(1);
	EXPECT_CALL(*_delegate, create_patch_completed(1))
		.Times(1);
	EXPECT_CALL(*_delegate, patch_exists_completed(2, true))
		.Times(1);
	EXPECT_CALL(*_delegate, update_patch_completed(3))
		.Times(1);
	EXPECT_CALL(*_delegate, get_patch_completed(4, Field(&MapPatch::creation_date, p2.creation_date)))
		.Times(1);
	EXPECT_CALL(*_delegate, delete_patch_completed(5))
		.Times(1);
	EXPECT_CALL(*_delegate, patch_exists_completed(6, false))
		.Times(1);
}

MATCHER_P(checkPatchStationsEq, p, "") {
	bool st_equal = true;
	if (arg.stations.size() == p.stations.size()) {
		for (int i = 0; i < arg.stations.size(); ++i) {
			if (arg.stations[i].name != p.stations[i].name) {
				st_equal = false;
				break;
			}
		}
	} else
		st_equal = false;
	return (arg.id == p.id) && st_equal;
}

const Station gen_station(const std::string name)
{
	return Station(0,
			name,
			name, // descr
			"", // address
			false, // in_car_buy_fuel
			"", // img_small
			"", // img_big
			0, // lon
			0, // lat
			1); // column_count
}

TEST_F(BCacheTests, AppendStationsTest)
{
	std::vector<Station> stations;
	for (int i = 0; i < 10; i++) {
		std::ostringstream qs;
		qs << "station_" << i;
		auto s = gen_station(qs.str());
		stations.push_back(std::move(s));
	}
	const auto pid = get_patch_id(0, 0);
	const auto p = MapPatch(pid,
			std::chrono::system_clock::from_time_t(555));
	const auto ps = MapPatchStations(pid,
			stations);
	_cache->create_patch(p);
	_cache->create_stations(stations);
	_cache->get_patch_with_stations(pid);
	_cache->delete_patch(pid);
	_cache->get_patch_with_stations(pid);
	EXPECT_CALL(*_delegate, create_patch_completed(0))
		.Times(1);
	EXPECT_CALL(*_delegate, create_stations_completed(1))
		.Times(1);
	EXPECT_CALL(*_delegate, get_patch_stations_completed(2, checkPatchStationsEq(ps)))
		.Times(1);
	EXPECT_CALL(*_delegate, delete_patch_completed(3))
		.Times(1);
	EXPECT_CALL(*_delegate, get_patch_unk_completed(4))
		.Times(1);
}
