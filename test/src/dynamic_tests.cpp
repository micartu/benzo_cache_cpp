//
// Created by Michael Artuerhof, 2019
//
#include <dobject.hpp>
#include <dbtext.hpp>
#include "gtest/gtest.h"

using namespace std;
using namespace benzo_cache;

class DynamicTest : public ::testing::Test {

protected:
  virtual void SetUp() {
  };

  virtual void TearDown() {
  };
};

TEST_F(DynamicTest, TextTests) {
	const char* tt = "just for fun";
	dobject o = dbtext(tt);
	dbtext t = o;
	EXPECT_EQ(t.get_text(), tt);
}
