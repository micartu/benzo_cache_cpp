//
// Created by Michael Artuerhof, 2019
//
#include <dobject.hpp>
#include <dbtext.hpp>
#include <dbvalue.hpp>
#include <dbquery.hpp>
#include <memory>
#include <functional>
#include "executor.hpp"
#include "gtest/gtest.h"

using namespace std;
using namespace benzo_cache;
using dbinteger = dbvalue<int>;
using dbdouble = dbvalue<double>;

class ExecutorTests : public ::testing::Test {

protected:
  virtual void SetUp() {
	  _db = std::make_unique<sqlite3pp::database>("dbfile.db");
	  const char* initq =
		  "create table if not exists user ("
		  "   _id integer primary key autoincrement not null,"
		  "   age int,"
		  "   name text,"
		  "   weight real"
		  ");";
	  _db->execute(initq);

	  _queue = std::make_unique<wqueue<psql>>();
  };

  virtual void TearDown() {
	  _db->execute("drop table if exists user;");
  };

  std::unique_ptr<wqueue<psql>> _queue;
  std::unique_ptr<sqlite3pp::database> _db;
};

TEST_F(ExecutorTests, FetchQueryTest)
{
	seqnum selectseq = -1;
	const char* n1 = "howler";
	const char* n2 = "nobody";
	const double weight = 49.9f;
	const int age = 32;
	auto fun = [=] (seqnum n, pquery q) {
		if (n == selectseq) {
			auto users = q->result();
			int i = 0;
			EXPECT_EQ(users.size(), 2);
			for (auto v : users) {
				EXPECT_EQ(v.size(), 3);
				dbinteger a = v[0];
				EXPECT_EQ(a.get_val(), age);
				dbtext name = v[1];
				auto n = (i == 0) ? n1 : n2;
				EXPECT_STREQ(name.get_text().c_str(), n);
				dbdouble w = v[2];
				EXPECT_NEAR(w.get_val(), weight, 0.1);
				++i;
			}
		}
	};
	auto e = std::make_unique<executor>(*_queue, fun);
	for (int i = 0; i < 2; i++) {
		auto n = (i == 0) ? n1 : n2;
		auto iq = std::make_shared<benzo_cache::dbquery>(*_db,
				"insert into user (age,name,weight) values (?,?,?);",
				obj_list{
				dbinteger(age),
				dbtext(n),
				dbdouble(weight)
				});
		auto seq = e->put(iq);
	}

	// fetch-query:
	auto fq = std::make_shared<benzo_cache::dbquery>(*_db,
			"select age,name,weight from user;",
			obj_list{dbinteger(), dbtext(), dbdouble()}, true);
	selectseq = e->put(fq);
}
