#include "bcacheimp.hpp"
#include "common_dtypes.hpp"
#include <sstream>

using namespace benzo_cache;

seqnum BCacheImpl::exists_patch(int32_t id)
{
	std::ostringstream qs;
	qs << "SELECT COUNT(*) FROM cmap_patch WHERE id = "
		<< (uint32_t)id << ";";
	return exists_common(qs.str(), [this](seqnum n, bool exists) {
			_delegate->patch_exists_completed(n, exists);
	});
}
