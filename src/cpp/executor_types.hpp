#pragma once

#include <functional>
#include "common_types.hpp"

namespace benzo_cache
{
	class sqltask;

	using psql = std::shared_ptr<sqltask>;
	using done_fun = std::function<void (seqnum, pquery)>;
}
