#include "bcacheimp.hpp"
#include "dobject.hpp"
#include "dbtext.hpp"
#include "dbvalue.hpp"
#include "dbquery.hpp"
#include "executor.hpp"
#include "init_db.hpp"
#include <sstream>

using namespace benzo_cache;

std::shared_ptr<Bcache> Bcache::create_with_path(const std::string& path,
		const bdelegate& delegate)
{
	return std::make_shared<BCacheImpl>(path, delegate);
}

BCacheImpl::BCacheImpl(const std::string& path,
		const bdelegate& delegate)
	: _queue(std::make_unique<wqueue<psql>>()),
	_db(std::make_unique<sqlite3pp::database>(path.c_str())),
	_delegate(delegate),
	_mutex(),
	_ops(),
	_exe(std::make_unique<executor>(*_queue,
				[this] (seqnum n, pquery q) { operation_completed(n, q); } ))
{
	_db->execute(initialize_db);
}

// exist-sections of cache
#include "bcache_exist.ipp"

// create-sections of cache
#include "bcache_create.ipp"

// get-sections of cache
#include "bcache_get.ipp"

// update-sections of cache
#include "bcache_update.ipp"

// delete-sections of cache
#include "bcache_delete.ipp"

void BCacheImpl::operation_completed(seqnum num, pquery q)
{
	std::lock_guard<std::mutex> guard(_mutex);
	auto f = _ops.find(num);
	if (f != _ops.end()) {
		(f->second)(q);
		_ops.erase(f);
	}
}
