#include "executor.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include "sqltask.hpp"
#include "dbquery.hpp"

using namespace benzo_cache;

executor::executor(wqueue<psql>& queue,
		done_fun done)
	: _queue(queue),
	_seq(0),
	_done(done),
	_mutex(),
	_thread(std::thread(&executor::run, this)) { }

executor::~executor()
{
	if (_thread.joinable()) {
		_queue.add(nullptr);
		_thread.join();
	}
}

void executor::run()
{
	for (;;) {
		auto task = _queue.pop_front();

		if (task == nullptr) // condition for exit
			return;

		auto q = task->getQuery();
		q->execute();
		_done(task->getSeq(), q);
	}
}

seqnum executor::put(pquery q)
{
	std::lock_guard<std::mutex> guard(_mutex);
	auto task = std::make_shared<sqltask>(q, _seq);
	_queue.add(std::move(task));
	return _seq++;
}
