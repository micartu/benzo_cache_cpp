#include "dbtext_data.hpp"

using namespace benzo_cache;

dbtext::dbtext()
	: dobject(m_data = new dbtext::data()) {
}

dbtext::dbtext(std::string const& t)
	: dobject(m_data = new dbtext::data(t)) {
}

dbtext::dbtext(const dbtext& another)
	: dobject(m_data = another.is_null() ?
		   	nullptr : static_cast<dbtext::data*>(another.get_data()->clone())) { }

dbtext& dbtext::operator = (const dbtext& another) {
	reset(m_data = another.is_null() ?
		   	nullptr : static_cast<dbtext::data*>(another.get_data()->clone()));
	return *this;
}

dbtext::dbtext(const dobject& another)
	: dobject(m_data = (dynamic_cast<const dbtext::data*>(another.get_data()) ?
				dynamic_cast<dbtext::data*>(another.get_data()->clone()) : nullptr)) { }

dbtext::~dbtext() { }

std::string dbtext::get_text() const {
	return m_data->get_text();
}

void dbtext::set_text(std::string const& dbtext) {
	m_data->set_text(dbtext);
}
