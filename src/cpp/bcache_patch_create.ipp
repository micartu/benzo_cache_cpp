#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"

// entities
#include "map_patch.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::create_patch(const MapPatch& p)
{
	// almost generated part of query:
	std::ostringstream qs;
	qs 	<< "INSERT INTO cmap_patch (" << fields_patch << ") VALUES ("
		<< (uint32_t)p.id << ","
		<< std::chrono::system_clock::to_time_t(p.creation_date)
		<< ");";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db, qs.str());
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->create_patch_completed(n);
	};
	return n;
}
