// common file with list of other sections
#include "bcacheimp.hpp"
#include "common_dtypes.hpp"

seqnum BCacheImpl::exists_common(const std::string& query,
		const std::function<void (seqnum, bool)>& f)
{
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			query,
			obj_list{dbinteger()}, true);
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [n, f] (pquery q) {
		auto r = q->result();
		bool exists;
		if (r.size() > 0) {
			dbinteger count = r[0][0];
			exists = (count.get_val() > 0);
		} else
			exists = false;
		f(n, exists);
	};
	return n;
}

// user
#include "bcache_usr_exist.ipp"

// transaction
#include "bcache_transaction_exist.ipp"

// webpage
#include "bcache_webpage_exist.ipp"

// map_patch
#include "bcache_patch_exist.ipp"
