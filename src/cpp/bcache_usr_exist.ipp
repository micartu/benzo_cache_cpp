#include "bcacheimp.hpp"
#include "common_dtypes.hpp"
#include <sstream>

using namespace benzo_cache;

seqnum BCacheImpl::exists_user(const std::string& login)
{
	std::ostringstream qs;
	qs << "SELECT COUNT(*) FROM cuser WHERE login = '"
		<< login << "';";
	return exists_common(qs.str(), [this](seqnum n, bool exists) {
			_delegate->user_exists_completed(n, exists);
	});
}
