#pragma once

#include <string>
#include "cow.h"

namespace benzo_cache {

class dobject
{
public:
    dobject();
    virtual ~dobject();

	/// copy constructor based on concrete other dynamic object
    dobject(const dobject& another);
	/// initializes data from given string based on type of object
    dobject(const dobject& another, const std::string& str);
    dobject& operator = (const dobject& another);

    bool is_null() const;
    dobject clone() const;

    class data;

    const data* get_data() const;

    const char* data_class() const;

    virtual std::string to_string() const;

	// operators
	operator std::string() const;
	operator const char*() const;

protected:
    dobject(data* new_data);
    void reset(data* new_data);

    void assert_not_null(const char* file, int line) const;

private:
    copy_on_write<data> m_data;
};

} // benzo_cache
