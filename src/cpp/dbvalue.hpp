#pragma once

#include "dobject.hpp"
#include <string>

namespace benzo_cache {

template<class TYPE>
class dbvalue : public dobject
{
public:
    dbvalue();
    dbvalue(TYPE v);
    dbvalue(const dbvalue<TYPE>& another);
	dbvalue& operator = (const dbvalue<TYPE>& another);

	dbvalue(const dobject& another);
	dbvalue& operator = (const dobject& another);

    virtual ~dbvalue() {}

    TYPE get_val() const;
    void set_val(TYPE val);
protected:
    // must be declared only in place visible to dbvalue only
    class data;
private:
	data* m_data;
};
} // benzo_cache

// implementation of template
#include "dbvalue.ipp"
