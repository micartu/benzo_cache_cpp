#pragma once

#include <memory> // std::shared_ptr

namespace benzo_cache {
template <class data_type>
class copy_on_write
{
public:
    copy_on_write(data_type* data)
        : m_data(data) {
    }
    data_type const* operator -> () const {
        return m_data.get();
    }
    data_type* operator -> () {
        if (m_data.unique())
            m_data.reset(new data_type(*m_data));
        return m_data.get();
    }
    bool operator ! () const {
		return !m_data;
	}
	void reset(data_type* p) {
		m_data.reset(p);
	}
    data_type const* get() const {
		return m_data.get();
	}
private:
    std::shared_ptr<data_type> m_data;
};
} // benzo_cache
