#include "blcacheimp.hpp"
#include "bcache.hpp"
#include "blistenercache.hpp"
#include <sstream>

#include "user.hpp"
#include "transaction.hpp"
#include "web_page.hpp"
#include "map_patch.hpp"
#include "station.hpp"
#include "map_patch_stations.hpp"

#define PROMISE(v) auto w = _delegate->promiseFor##v()
#define GETPROM() w->get_future().get()

using namespace benzo_cache;

std::shared_ptr<Blcache> Blcache::create_with_path(const std::string& path)
{
	return std::make_shared<BlCacheImpl>(path);
}

BlCacheImpl::BlCacheImpl(const std::string& path)
	: _delegate(std::make_shared<BListenerCache>()),
	_cache(Bcache::create_with_path(path, _delegate))
{
}

// ************** user ***************

bool BlCacheImpl::exists_user(const std::string& login)
{
	PROMISE(Bool);
	_cache->exists_user(login);
	return GETPROM();
}

void BlCacheImpl::create_user(const User& u)
{
	PROMISE(Void);
	_cache->create_user(u);
	GETPROM();
}

void BlCacheImpl::update_user(const User& u)
{
	PROMISE(Void);
	_cache->update_user(u);
	GETPROM();
}

std::optional<User> BlCacheImpl::get_user(const std::string & login)
{
	PROMISE(User);
	_cache->get_user(login);
	return GETPROM();
}

void BlCacheImpl::delete_user(const std::string& login)
{
	PROMISE(Void);
	_cache->delete_user(login);
	GETPROM();
}

// ************** transaction ***************

bool BlCacheImpl::exists_transaction(const std::string& login, const std::string& rrn)
{
	PROMISE(Bool);
	_cache->exists_transaction(login, rrn);
	return GETPROM();
}

void BlCacheImpl::create_transaction(const std::string& login, const Transaction& t)
{
	PROMISE(Void);
	_cache->create_transaction(login, t);
	GETPROM();
}

std::optional<Transaction> BlCacheImpl::get_transaction(const std::string & login, const std::string& rrn)
{
	PROMISE(Transaction);
	_cache->get_transaction(login, rrn);
	return GETPROM();
}

void BlCacheImpl::update_transaction(const Transaction& t)
{
	PROMISE(Void);
	_cache->update_transaction(t);
	GETPROM();
}

void BlCacheImpl::delete_transaction(const std::string& rrn)
{
	PROMISE(Void);
	_cache->delete_transaction(rrn);
	GETPROM();
}

// ************** webpage ***************

bool BlCacheImpl::exists_webpage(const std::string& login, const std::string& name)
{
	PROMISE(Bool);
	_cache->exists_webpage(login, name);
	return GETPROM();
}

void BlCacheImpl::create_webpage(const std::string& login, const WebPage& p)
{
	PROMISE(Void);
	_cache->create_webpage(login, p);
	GETPROM();
}

std::optional<WebPage> BlCacheImpl::get_webpage(const std::string & login, const std::string& name)
{
	PROMISE(WebPage);
	_cache->get_webpage(login, name);
	return GETPROM();
}

void BlCacheImpl::update_webpage(const std::string& login, const WebPage& p)
{
	PROMISE(Void);
	_cache->update_webpage(login, p);
	GETPROM();
}

void BlCacheImpl::delete_webpage(const std::string & login, const std::string& name)
{
	PROMISE(Void);
	_cache->delete_webpage(login, name);
	GETPROM();
}

// ************** patch ***************

bool BlCacheImpl::exists_patch(int32_t id)
{
	PROMISE(Bool);
	_cache->exists_patch(id);
	return GETPROM();
}


void BlCacheImpl::create_patch(const MapPatch& p)
{
	PROMISE(Void);
	_cache->create_patch(p);
	GETPROM();
}

std::optional<MapPatch> BlCacheImpl::get_patch(int32_t id)
{
	PROMISE(MapPatch);
	_cache->get_patch(id);
	return GETPROM();
}

void BlCacheImpl::update_patch(const MapPatch& p)
{
	PROMISE(Void);
	_cache->update_patch(p);
	GETPROM();
}

void BlCacheImpl::delete_patch(int32_t id)
{
	PROMISE(Void);
	_cache->delete_patch(id);
	GETPROM();
}

// ************** stations ***************

void BlCacheImpl::create_stations(const std::vector<Station>& ss)
{
	PROMISE(Void);
	_cache->create_stations(ss);
	GETPROM();
}

std::optional<MapPatchStations> BlCacheImpl::get_patch_with_stations(int32_t id)
{
	PROMISE(MapPatchStations);
	_cache->get_patch_with_stations(id);
	return GETPROM();
}

#undef PROMISE
#undef GETPROM
