#include "bcacheimp.hpp"
#include <sstream>

using namespace benzo_cache;

seqnum BCacheImpl::delete_webpage(const std::string& login, const std::string& name)
{
	// find out webpage's id query:
	std::ostringstream qs;
	qs << "DELETE FROM cwebpage WHERE _id = "
		<< "(SELECT cwebpage._id FROM cusr_webpage "
		<< " JOIN cuser ON cusr_webpage.usr_id = cuser._id "
		<< " JOIN cwebpage ON cusr_webpage.wp_id = cwebpage._id "
		<< " WHERE cuser.login = '" << login << "'"
		<< " AND cwebpage.pname = '" << name << "');";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			qs.str());
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->delete_webpage_completed(n);
	};
	return n;
}
