#include "bcacheimp.hpp"
#include <sstream>

using namespace benzo_cache;

seqnum BCacheImpl::delete_user(const std::string& login)
{
	std::ostringstream qs;
	qs << "DELETE FROM cuser WHERE login = '" << login << "';";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			qs.str());
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->delete_user_completed(n);
	};
	return n;
}
