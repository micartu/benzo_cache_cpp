// common file with list of other sections

// user
#include "bcache_user_update.ipp"

// transaction
#include "bcache_trans_update.ipp"

// webpage
#include "bcache_webpage_update.ipp"

// patch
#include "bcache_patch_update.ipp"
