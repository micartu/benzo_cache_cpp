#pragma once

#include "dbvalue.hpp"
#include "dobject_data.hpp"
#include <typeinfo>
#include <string>
#include <sstream>

namespace benzo_cache {

template<class T>
class dbvalue<T>::data : public dobject::data
{
public:
	// constructors/destructors
    data()
        : m_value{}
   	{
    }
    data(T value)
        : m_value(value)
   	{
    }

	// base class overrides:

    virtual std::string to_string() const override
	{
        return std::to_string(m_value);
    }

	virtual const char* class_name() const override { return typeid(m_value).name(); }

	virtual dobject::data* clone() const override { return new dbvalue::data(*this); }

	virtual dobject::data* from_string(const std::string str) const override
	{
		T number;
		std::stringstream iss(str);
		iss >> number;
		return new dbvalue<T>::data(number);
	}

	// additional methods:

	/// sets/gets the value of container
    T get_val() const { return m_value; }
    void set_val(T val) { m_value = val; }
private:
    T m_value;
};

} // benzo_cache
