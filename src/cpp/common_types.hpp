#pragma once

#include <vector>
#include <memory>
#include "dobject.hpp"

namespace benzo_cache
{
	using vector_objs = std::vector<dobject>;
	using seqnum = int64_t;
	class dbquery;
	using pquery = std::shared_ptr<dbquery>;
	using obj_list = std::initializer_list<dobject>;
}
