#pragma once

namespace benzo_cache
{
	const char* fields_user =
		"   login,"
		"   name,"
		"   phone_number,"
		"   lastname,"
		"   middle_name,"
		"   crd_pin,"
		"   card,"
		"   email,"
		"   codeword,"
		"   fav_fuel,"
		"   balance,"
		"   menu_refresh_date,"
		"   card_blocked,"
		// loyality part
		"   cpa_crd_code,"
		"   cpa_crd_rub,"
		"   cpa_prt_code,"
		"   cpa_prt_name,"
		"   cpa_prt_ref1,"
		"   cpa_prt_ref2,"
		"   cpa_prt_ref3,"
		"   cpa_prt_subid"
		"";

	const char* fields_transaction =
		"   rrn,"
		"   date,"
		"   trn_type,"
		"   brand_name,"
		"   location,"
		"   product_name,"
		"   product_quantity,"
		"   trn_bb,"
		"   trn_value,"
		"   points,"
		"   status,"
		"   trn_comment,"
		"   convert_bonus_date,"
		"   discount_sign,"
		"   discount_value,"
		"   discount_value_currency,"
		"   channel,"
		"   inner_type"
		"";


	const char* fields_webpage =
		"   pname,"
		"   cdate,"
		"   content"
		"";

	const char* fields_patch =
		"   id,"
		"   cdate"
		"";

	const char* fields_station =
		"   sname,"
		"   descr,"
		"   addr,"
		"   in_car_buy_fuel,"
		"   img_small,"
		"   img_big,"
		"   lon,"
		"   lat,"
		"   column_count"
		"";

} // benzo_cache
