// common file with list of other sections

// user creation
#include "bcache_usr_create.ipp"

// transaction creation
#include "bcache_trans_create.ipp"

// webpage
#include "bcache_webpage_create.ipp"

// patch
#include "bcache_patch_create.ipp"

// stations
#include "bcache_stations_create.ipp"
