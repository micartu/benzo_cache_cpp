#include "dobject_data.hpp"
#include <stdexcept>
#include <sstream>
#include <ostream>

using namespace benzo_cache;

dobject::dobject()
	: m_data(nullptr)
{
}

dobject::~dobject()
{
}

dobject::dobject(dobject::data* new_data)
    : m_data(new_data)
{
}

dobject::dobject(const dobject& another)
    : m_data(another.is_null() ? nullptr : another.m_data->clone())
{
}

dobject::dobject(const dobject& another, const std::string& str)
	: m_data(another.m_data->from_string(str))
{
}

dobject& dobject::operator = (const dobject& another)
{
    m_data.reset(another.is_null() ? nullptr : another.m_data->clone());
    return *this;
}

bool dobject::is_null() const
{
    return !m_data;
}

dobject dobject::clone() const
{
    return is_null() ? dobject() : dobject(m_data->clone());
}

const dobject::data* dobject::get_data() const
{
    return m_data.get();
}

const char* dobject::data_class() const
{
    return is_null() ? "null" : m_data->class_name();
}

void dobject::reset(dobject::data* new_data)
{
    m_data.reset(new_data);
}

std::string dobject::to_string() const
{
	return m_data->to_string();
}

dobject::operator std::string() const
{
	return m_data->to_string();
}

dobject::operator const char*() const
{
	return m_data->to_string().c_str();
}

void dobject::assert_not_null(const char* file, int line) const
{
    if (is_null())
    {
        std::stringstream output;
        output << "Assert 'dobject is not null' failed at file: '" << file << "' line: " << line;
        throw std::runtime_error(output.str());
    }
}
