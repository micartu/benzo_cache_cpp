#pragma once

#include <string>
#include <sqlite3pp.h>
#include <initializer_list>
#include "common_types.hpp"

namespace benzo_cache
{
	/// ! used for organization of queries to sqlite
	class dbquery
	{
	public:
			// Constructor
			dbquery(sqlite3pp::database& db,
					std::vector<std::string>&& queries,
					std::vector<bool>&& fetches,
					std::vector<vector_objs>&& params);
			dbquery(sqlite3pp::database& db,
					std::vector<std::string>&& queries);
			dbquery(sqlite3pp::database& db,
					std::initializer_list<std::string> queries,
					std::initializer_list<bool> fetches,
					std::initializer_list<std::initializer_list<dobject>> args);
			dbquery(sqlite3pp::database& db,
				   	std::string query,
					std::initializer_list<dobject> args,
					bool fetch = false);
			dbquery(sqlite3pp::database& db,
				   	std::string query,
				   	std::vector<dobject> params = std::vector<dobject>());

			// Destructor
			~dbquery();

			// public functions:

			/// executes given queries
			void execute();

			/// returns the result of the last fetch query
			const std::vector<vector_objs>& result() {
				assert(!_result.empty());
				return _result.back();
			}

			/// returns results of fetch queries
			const std::vector<std::vector<vector_objs>>& results() {
				return _result;
			}
	private:
			/// function checks if we need to save the last inserted row-id
			/// at given non-fetch query positions
			void check_row_ids(std::vector<bool>& grab_rid);
			/// replaces all parameters with real values
			std::string prepare_query(const std::string& query) const;
			/// a link to database itself
			sqlite3pp::database& _db;
			/// query string for database
			std::vector<std::string> _queries;
			/// is it a fetch query (select, _fetch = true) or
			/// should it be simply executed (_fetch = false)
			std::vector<bool> _fetches;
			/// results vectors, every entry
			/// would contain data if corresponding _fetch member equals true
			std::vector<std::vector<vector_objs>> _result;
			/// params for queries
			std::vector<vector_objs> _params;
	};
}
