#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_fun.hpp"

// entities
#include "station.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::create_stations(const std::vector<Station>& ss)
{
	std::vector<std::string> queries;
	std::ostringstream qs;
	int i = 0;
	for (auto s : ss) {
		qs 	<< "INSERT INTO cstation (" <<	fields_station << ") VALUES ("
			<< "'" << s.name << "'" << ","
			<< "'" << s.descr << "'" << ","
			<< "'" << s.address << "'" << ","
			<< (s.in_car_buy_fuel ? 1 : 0) << ","
			<< "'" << s.img_small << "'" << ","
			<< "'" << s.img_big << "'" << ","
			<< s.lon << ","
			<< s.lat << ","
			<< s.column_count
			<< ");";
		queries.push_back(qs.str());
		qs.str("");
		qs.clear();
		qs << "INSERT INTO cpatch_station (patch_id, station_id) VALUES ("
			<< get_patch_id(s.lon, s.lat) << ","
			<< "%row_id(" << i * 2 << ")"
			<< ");";
		queries.push_back(qs.str());
		qs.str("");
		qs.clear();
		++i;
	}
	auto q = std::make_shared<benzo_cache::dbquery>(*_db, std::move(queries));
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->create_stations_completed(n);
	};
	return n;
}
