#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "map_patch.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::get_patch(int32_t id)
{
	std::ostringstream qs;
	qs << "SELECT " << fields_patch << " FROM cmap_patch WHERE id = "
		<< (uint32_t)id << ";";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			qs.str(),
			obj_list{
			dbi32(), // id
			dbi64()  // cdate
			}, true);
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		auto res = q->result();
		if (res.size() > 0) {
			auto r = res[0];
			auto p = MapPatch(
					DBI32(r, 0), // id
					std::chrono::system_clock::from_time_t(DBI64(r, 1)) // creation_date
					);
			_delegate->get_patch_completed(n, p);
		} else
			_delegate->get_patch_unk_completed(n);
	};
	return n;
}
