#pragma once

#include <memory>
#include <future>
#include <optional>

namespace benzo_cache
{
	using fOptUser = std::promise<std::optional<User>>;
	using fOptTransaction = std::promise<std::optional<Transaction>>;
	using fOptWebPage = std::promise<std::optional<WebPage>>;
	using fOptMapPatch = std::promise<std::optional<MapPatch>>;
	using fOptMapPatchStations = std::promise<std::optional<MapPatchStations>>;
	using optBool = std::promise<bool>;
	using optVoid = std::promise<void>;

} // benzo_cache
