#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "map_patch.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::update_patch(const MapPatch& p)
{
	std::ostringstream qs;
	qs << "UPDATE cmap_patch SET "
		<< "cdate = " << std::chrono::system_clock::to_time_t(p.creation_date)
		<< " WHERE id = " << (uint32_t)p.id << ""
		<< ";";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db, qs.str());
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->update_patch_completed(n);
	};
	return n;
}
