#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "user.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::get_transaction(const std::string& login, const std::string& rrn)
{
	std::ostringstream qs;
	qs << "SELECT " << fields_transaction << " FROM cusr_transaction "
		<< " JOIN cuser ON cusr_transaction.usr_id = cuser._id "
		<< " JOIN ctransaction ON cusr_transaction.tra_id = ctransaction._id "
		<< " WHERE cuser.login = '" << login << "'"
		<< " AND ctransaction.rrn = '" << rrn << "';";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			qs.str(),
			obj_list{
			dbtext(), // rrn
			dbi64(), // date
			dbtext(), // trn_type
			dbtext(), // brand_name
			dbtext(), // location
			dbtext(), // product_name
			dbdouble(), // product_quantity
			dbi32(), // trn_bb
			dbi32(), // trn_value
			dbi32(), // points
			dbtext(), // status
			dbtext(), // trn_comment
			dbi64(), // convert_bonus_date
			dbi32(), // discount_sign
			dbi32(), // discount_value
			dbi32(), // discount_value_currency
			dbtext(), // channel
			dbtext(), // innertype
			}, true);
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		auto res = q->result();
		if (res.size() > 0) {
			auto r = res[0];
			auto t = Transaction(
					DBTEX(r, 0), // rrn
					std::chrono::system_clock::from_time_t(DBI64(r, 1)), // date
					DBTEX(r, 2), // trn_type
					DBTEX(r, 3), // brand_name
					DBTEX(r, 4), // location
					DBTEX(r, 5), // product_name
					DBDBL(r, 6), // product_quantity
					DBI32(r, 7), // trn_bb
					DBI32(r, 8), // trn_value
					DBI32(r, 9), // points
					DBTEX(r, 10), // status
					DBTEX(r, 11), // trn_comment
					std::chrono::system_clock::from_time_t(DBI64(r, 12)), // convert_bonus_date
					DBI32(r, 13), // discount_sign
					DBI32(r, 14), // discount_value
					DBI32(r, 15), // discount_value_currency
					DBTEX(r, 16), // channel
					DBTEX(r, 17)  // innertype
					);
			_delegate->get_transaction_completed(n, t);
		} else
			_delegate->get_transaction_unk_completed(n);
	};
	return n;
}
