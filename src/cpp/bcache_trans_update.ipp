#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "transaction.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::update_transaction(const Transaction& t)
{
	std::ostringstream qs;
	qs << "UPDATE ctransaction SET "
		<< "date = " << std::chrono::system_clock::to_time_t(t.creation_date) << ","
		<< "trn_type = '" << t.trn_type << "'" << ","
		<< "brand_name = '" << t.brand_name << "'" << ","
		<< "location = '" << t.location << "'" << ","
		<< "product_name = '" << t.product_name << "'" << ","
		<< "product_quantity = " << t.product_quantity << ","
		<< "trn_bb = " << t.trn_bb << ","
		<< "trn_value = " << t.trn_value << ","
		<< "points = " << t.points << ","
		<< "status = '" << t.status << "'" << ","
		<< "trn_comment = '" << t.trn_comment << "'" << ","
		<< "convert_bonus_date = " << std::chrono::system_clock::to_time_t(t.convert_bonus_date) << ","
		<< "discount_sign = " << t.discount_sign << ","
		<< "discount_value = " << t.discount_value << ","
		<< "discount_value_currency = " << t.discount_value_currency << ","
		<< "channel = '" << t.channel << "'" << ","
		<< "inner_type = '" << t.innertype << "'"
		<< " WHERE rrn = '" << t.rrn << "'"
		<< ";";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db, qs.str());
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->update_transaction_completed(n);
	};
	return n;
}
