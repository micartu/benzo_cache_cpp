#include "common_fun.hpp"
#include <cmath> // std::abs

#define dLon 0.0710678041f
#define dLat 0.0713678069f

uint32_t get_patch_id(double lon, double lat)
{
    int a = (lon >= 0) ? 1 : 0;
    int b = ((lat > 0) ? 1 : 0) << 1;
    int llon = ((int)(abs(lon) / dLon)) << 3;
    int llat = ((int)(abs(lat) / dLat)) << 15;

    return a | b | llon | llat;
}

#undef dLon
#undef dLat
