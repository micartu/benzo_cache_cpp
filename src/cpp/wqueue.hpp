#pragma once

#include <list>
#include <mutex>
#include <condition_variable>

#define WAIT_FOR_QUEUE() \
		std::unique_lock<std::mutex> guard(_mutex); \
		_condv.wait(guard, [this] { return (_queue.size() != 0); })

namespace benzo_cache
{

	template <typename T> class wqueue
	{
		public:
			wqueue() { }

			~wqueue() { }

			void wake()
			{
				std::lock_guard<std::mutex> guard(_mutex);
				_condv.notify_one();
			}

			void add(T&& item)
			{
				std::lock_guard<std::mutex> guard(_mutex);
				_queue.push_back(std::move(item));
				_condv.notify_one();
			}

			T pop_front()
			{
				WAIT_FOR_QUEUE();
				T item = _queue.front();
				_queue.pop_front();
				return item;
			}

			void remove(T t)
			{
				WAIT_FOR_QUEUE();
				_queue.remove(t);
			}

			T front()
			{
				WAIT_FOR_QUEUE();
				T item = _queue.front();
				return item;
			}

			auto size()
			{
				std::lock_guard<std::mutex> guard(_mutex);
				auto size = _queue.size();
				return size;
			}

		private:
			std::list<T> _queue;
			std::mutex _mutex;
			std::condition_variable _condv;
	};
}

#undef WAIT_FOR_QUEUE
