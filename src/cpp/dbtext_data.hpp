#pragma once

#include "dbtext.hpp"
#include "dobject_data.hpp"

namespace benzo_cache {

class dbtext::data : public dobject::data
{
public:
	// constructors/destructors
    data()
        : m_text("") {
    }
    data(std::string const& name)
        : m_text(name) {
    }

	// base class overrides:

    virtual std::string to_string() const override {
        return m_text;
    }

	virtual const char* class_name() const override { return "dbtext"; }

	virtual dobject::data* clone() const override { return new dbtext::data(*this); }

	virtual dobject::data* from_string(const std::string str) const override
	{
		return new dbtext::data(str);
	}

	// additional methods:

	/// sets/gets the name of the dbtext
    std::string get_text() const { return m_text; }
    void set_text(std::string const& text) { m_text = text; }
private:
    std::string m_text;
};

} // benzo_cache
