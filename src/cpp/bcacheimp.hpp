#pragma once

#include "bcache.hpp"
#include <memory>
#include <string>
#include <mutex>
#include <unordered_map>
#include "executor_types.hpp"
#include "bcache_listener.hpp"

namespace sqlite3pp { class database; }

namespace benzo_cache
{
	class executor;
	template <typename T> class wqueue;

	using bdelegate = std::shared_ptr<BcacheListener>;

	class BCacheImpl : public Bcache
	{
		public:
			// Constructor
			BCacheImpl(const std::string& path, const bdelegate& delegate);

			// general methods
			virtual seqnum exists_user(const std::string& login);
			virtual seqnum exists_transaction(const std::string& login, const std::string& rrn);
			virtual seqnum exists_webpage(const std::string& login, const std::string& name);
			virtual seqnum exists_patch(int32_t id);

			// creation methods
			virtual seqnum create_user(const User& u);
			virtual seqnum create_transaction(const std::string& login, const Transaction& t);
			virtual seqnum create_webpage(const std::string& login, const WebPage& p);
			virtual seqnum create_patch(const MapPatch& p);
			virtual seqnum create_stations(const std::vector<Station>& ss);

			// get methods
			virtual seqnum get_user(const std::string & login);
			virtual seqnum get_transaction(const std::string& login, const std::string& rrn);
			virtual seqnum get_webpage(const std::string& login, const std::string& name);
			virtual seqnum get_patch(int32_t id);
			virtual seqnum get_patch_with_stations(int32_t id);

			// update methods
			virtual seqnum update_user(const User& u);
			virtual seqnum update_transaction(const Transaction& t);
			virtual seqnum update_webpage(const std::string& login, const WebPage& p);
			virtual seqnum update_patch(const MapPatch& p);

			// deletion methods
			virtual seqnum delete_user(const std::string & login);
			virtual seqnum delete_transaction(const std::string& rrn);
			virtual seqnum delete_webpage(const std::string& login, const std::string& name);
			virtual seqnum delete_patch(int32_t id);

		private:
			inline seqnum exists_common(const std::string& query,
					const std::function<void (seqnum, bool)>& f);
			void operation_completed(seqnum num, pquery q);

			std::unique_ptr<wqueue<psql>> _queue;
			std::unique_ptr<sqlite3pp::database> _db;
			bdelegate _delegate;
			std::mutex _mutex;
			std::unordered_map<seqnum, std::function<void (pquery)>> _ops;
			std::unique_ptr<executor> _exe;
	};

} // benzo_cache
