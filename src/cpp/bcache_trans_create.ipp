#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"

// entities
#include "transaction.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::create_transaction(const std::string& login, const Transaction& t)
{
	// transaction creation query:
	std::ostringstream tra_qs;
	tra_qs << "INSERT INTO ctransaction (" <<	fields_transaction << ") VALUES ("
		<< "'" << t.rrn << "'" << ","
		<< std::chrono::system_clock::to_time_t(t.creation_date) << ","
		<< "'" << t.trn_type << "'" << ","
		<< "'" << t.brand_name << "'" << ","
		<< "'" << t.location << "'" << ","
		<< "'" << t.product_name << "'" << ","
		<< t.product_quantity << ","
		<< t.trn_bb << ","
		<< t.trn_value << ","
		<< t.points << ","
		<< "'" << t.status << "'" << ","
		<< "'" << t.trn_comment << "'" << ","
		<< std::chrono::system_clock::to_time_t(t.convert_bonus_date) << ","
		<< t.discount_sign << ","
		<< t.discount_value << ","
		<< t.discount_value_currency << ","
		<< "'" << t.channel << "'" << ","
		<< "'" << t.innertype << "'"
		<< ");";
	// find out user's id query:
	std::ostringstream usr_id_qs;
	usr_id_qs << "SELECT _id FROM cuser WHERE login = "
		<< "'" << login << "'" << ";";
	// and insert that data into aggregate table cusr_transaction:
	const std::string usr_tra_qs =
		"INSERT INTO cusr_transaction (usr_id, tra_id) VALUES (%s(1,0), %row_id(0));";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			std::initializer_list<std::string> {
				tra_qs.str(),
				usr_id_qs.str(),
				usr_tra_qs
			},
			std::initializer_list<bool> {
				false, // no fetch
				true, // fetch
				false // no fetch
			},
			std::initializer_list<obj_list> {
				{}, // nothing here (would be only row_id)
				{
				dbtext(), // id
				},
				{}, // nothing, only INSERT-operation
			});
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->create_transaction_completed(n);
	};
	return n;
}
