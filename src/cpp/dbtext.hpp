#pragma once

#include "dobject.hpp"
#include <string>

namespace benzo_cache {
class dbtext : public dobject
{
public:
    dbtext();
    dbtext(std::string const& t);
    dbtext(const dbtext& another);
	dbtext& operator = (const dbtext& another);

	dbtext(const dobject& another);
	dbtext& operator = (const dobject& another);

    virtual ~dbtext();

    std::string get_text() const;
    void set_text(std::string const& dbtext);
protected:
    // must be declared only in place visible to dbtext only
    class data;
private:
	data* m_data;
};
} // benzo_cache
