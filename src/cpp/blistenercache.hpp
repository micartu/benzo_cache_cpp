#pragma once

#include <memory>
#include <string>
#include <future>
#include <optional>
#include "bcache_listener.hpp"
#include "blistenercache_types.hpp"

namespace benzo_cache
{
	class BListenerCache : public BcacheListener
	{
		public:
			// Constructor
			BListenerCache();

			// getters:
			std::shared_ptr<optBool> promiseForBool();
			std::shared_ptr<optVoid> promiseForVoid();
			std::shared_ptr<fOptUser> promiseForUser();
			std::shared_ptr<fOptWebPage> promiseForWebPage();
			std::shared_ptr<fOptMapPatch> promiseForMapPatch();
			std::shared_ptr<fOptMapPatchStations> promiseForMapPatchStations();
			std::shared_ptr<fOptTransaction> promiseForTransaction();

			// listener methods
			virtual void user_exists_completed(int64_t id, bool exists);

			virtual void transaction_exists_completed(int64_t id, bool exists);

			virtual void webpage_exists_completed(int64_t id, bool exists);

			virtual void patch_exists_completed(int64_t id, bool exists);

			/** creation-entries */
			virtual void create_user_completed(int64_t id);

			virtual void create_transaction_completed(int64_t id);

			virtual void create_webpage_completed(int64_t id);

			virtual void create_patch_completed(int64_t id);

			virtual void create_stations_completed(int64_t id);

			/** get-entries */
			virtual void get_user_completed(int64_t id, const User & u);

			virtual void get_user_unk_completed(int64_t id);

			virtual void get_transaction_completed(int64_t id, const Transaction & t);

			virtual void get_transaction_unk_completed(int64_t id);

			virtual void get_webpage_completed(int64_t id, const WebPage & p);

			virtual void get_webpage_unk_completed(int64_t id);

			virtual void get_patch_completed(int64_t id, const MapPatch & p);

			virtual void get_patch_unk_completed(int64_t id);

			virtual void get_patch_stations_completed(int64_t id, const MapPatchStations & p);

			/** update-entries */
			virtual void update_user_completed(int64_t id);

			virtual void update_transaction_completed(int64_t id);

			virtual void update_webpage_completed(int64_t id);

			virtual void update_patch_completed(int64_t id);

			/** deletion-entries */
			virtual void delete_user_completed(int64_t id);

			virtual void delete_transaction_completed(int64_t id);

			virtual void delete_webpage_completed(int64_t id);

			virtual void delete_patch_completed(int64_t id);

		private:
			// common functions:
			void wait_bool(bool exists);
			void wait_void();

			// members:
			std::shared_ptr<optBool> _bool_promise;
			std::shared_ptr<optVoid> _void_promise;
			std::shared_ptr<fOptUser> _usr_promise;
			std::shared_ptr<fOptWebPage> _webpage_promise;
			std::shared_ptr<fOptMapPatch> _map_patch_promise;
			std::shared_ptr<fOptMapPatchStations> _map_patch_stations_promise;
			std::shared_ptr<fOptTransaction> _trans_promise;
	};

} // benzo_cache
