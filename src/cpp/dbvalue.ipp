#include "dbvalue_data.hpp"

using namespace benzo_cache;

template <typename T>
dbvalue<T>::dbvalue()
	: dobject(m_data = new dbvalue<T>::data()) {
}

template <typename T>
dbvalue<T>::dbvalue(T t)
	: dobject(m_data = new dbvalue<T>::data(t)) {
}

template <typename T>
dbvalue<T>::dbvalue(const dbvalue<T>& another)
	: dobject(m_data = another.is_null() ?
		   	nullptr : static_cast<dbvalue::data*>(another.get_data()->clone())) { }

template <typename T>
dbvalue<T>& dbvalue<T>::operator = (const dbvalue<T>& another) {
	reset(m_data = another.is_null() ?
		   	nullptr : static_cast<dbvalue<T>::data*>(another.get_data()->clone()));
	return *this;
}

template <typename T>
dbvalue<T>::dbvalue(const dobject& another)
	: dobject(m_data = (dynamic_cast<const dbvalue<T>::data*>(another.get_data()) ?
				dynamic_cast<dbvalue<T>::data*>(another.get_data()->clone()) : nullptr)) { }

template <typename T>
T dbvalue<T>::get_val() const {
	return m_data->get_val();
}

template <typename T>
void dbvalue<T>::set_val(T val) {
	m_data->set_val(val);
}
