#pragma once

#include <thread>
#include <mutex>
#include "wqueue.hpp"
#include "executor_types.hpp"

namespace benzo_cache
{
	class executor
	{
		public:
			executor(wqueue<psql>& queue,
					done_fun done);
			~executor();

			seqnum put(pquery task);

		private:
			/// function to be executed
			void run();

			wqueue<psql>& _queue;
			seqnum _seq;

			done_fun _done;

			// mutex for seq
			std::mutex _mutex;
			std::thread _thread;
	};
}
