#include "blistenercache.hpp"
#include "user.hpp"
#include "transaction.hpp"
#include "web_page.hpp"
#include "map_patch.hpp"
#include "map_patch_stations.hpp"

using namespace benzo_cache;

BListenerCache::BListenerCache()
	: _bool_promise(nullptr),
	_void_promise(nullptr),
	_usr_promise(nullptr),
	_webpage_promise(nullptr),
	_map_patch_promise(nullptr),
	_map_patch_stations_promise(nullptr),
	_trans_promise(nullptr)
{
}

std::shared_ptr<optBool> BListenerCache::promiseForBool()
{
	_bool_promise = std::make_shared<optBool>();
	return _bool_promise;
}

std::shared_ptr<optVoid> BListenerCache::promiseForVoid()
{
	_void_promise = std::make_shared<optVoid>();
	return _void_promise;
}

std::shared_ptr<fOptUser> BListenerCache::promiseForUser()
{
	_usr_promise = std::make_shared<fOptUser>();
	return _usr_promise;
}

std::shared_ptr<fOptTransaction> BListenerCache::promiseForTransaction()
{
	_trans_promise = std::make_shared<fOptTransaction>();
	return _trans_promise;
}

std::shared_ptr<fOptWebPage> BListenerCache::promiseForWebPage()
{
	_webpage_promise = std::make_shared<fOptWebPage>();
	return _webpage_promise;
}

std::shared_ptr<fOptMapPatch> BListenerCache::promiseForMapPatch()
{
	_map_patch_promise = std::make_shared<fOptMapPatch>();
	return _map_patch_promise;
}

std::shared_ptr<fOptMapPatchStations> BListenerCache::promiseForMapPatchStations()
{
	_map_patch_stations_promise = std::make_shared<fOptMapPatchStations>();
	return _map_patch_stations_promise;
}

void BListenerCache::wait_bool(bool exists)
{
	assert(_bool_promise != nullptr);
	_bool_promise->set_value(exists);
}

void BListenerCache::user_exists_completed(int64_t id, bool exists)
{
	wait_bool(exists);
}

void BListenerCache::transaction_exists_completed(int64_t id, bool exists)
{
	wait_bool(exists);
}

void BListenerCache::webpage_exists_completed(int64_t id, bool exists)
{
	wait_bool(exists);
}

void BListenerCache::patch_exists_completed(int64_t id, bool exists)
{
	wait_bool(exists);
}

void BListenerCache::wait_void()
{
	assert(_void_promise != nullptr);
	_void_promise->set_value();
}

void BListenerCache::create_user_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::create_transaction_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::create_webpage_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::create_patch_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::create_stations_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::get_user_completed(int64_t id, const User & u)
{
	assert(_usr_promise != nullptr);
	_usr_promise->set_value(u);
}

void BListenerCache::get_user_unk_completed(int64_t id)
{
	assert(_usr_promise != nullptr);
	_usr_promise->set_value(std::nullopt);
}

void BListenerCache::get_transaction_completed(int64_t id, const Transaction & t)
{
	assert(_trans_promise != nullptr);
	_trans_promise->set_value(t);
}

void BListenerCache::get_transaction_unk_completed(int64_t id)
{
	assert(_trans_promise != nullptr);
	_trans_promise->set_value(std::nullopt);
}

void BListenerCache::get_webpage_completed(int64_t id, const WebPage & p)
{
	assert(_webpage_promise != nullptr);
	_webpage_promise->set_value(p);
}

void BListenerCache::get_webpage_unk_completed(int64_t id)
{
	assert(_webpage_promise != nullptr);
	_webpage_promise->set_value(std::nullopt);
}

void BListenerCache::get_patch_completed(int64_t id, const MapPatch & p)
{
	assert(_map_patch_promise != nullptr);
	_map_patch_promise->set_value(p);
}

void BListenerCache::get_patch_unk_completed(int64_t id)
{
	if (_map_patch_promise != nullptr) {
		_map_patch_promise->set_value(std::nullopt);
		_map_patch_promise = nullptr;
	} else if (_map_patch_stations_promise != nullptr) {
		_map_patch_stations_promise->set_value(std::nullopt);
		_map_patch_stations_promise = nullptr;
	}
}

void BListenerCache::get_patch_stations_completed(int64_t id, const MapPatchStations & p)
{
	if (_map_patch_stations_promise != nullptr) {
		_map_patch_stations_promise->set_value(p);
		_map_patch_stations_promise = nullptr;
	}
}

void BListenerCache::update_user_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::update_transaction_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::update_webpage_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::update_patch_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::delete_user_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::delete_transaction_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::delete_webpage_completed(int64_t id)
{
	wait_void();
}

void BListenerCache::delete_patch_completed(int64_t id)
{
	wait_void();
}
