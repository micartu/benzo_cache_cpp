#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "web_page.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::get_webpage(const std::string& login, const std::string& name)
{
	std::ostringstream qs;
	qs << "SELECT " << fields_webpage << " FROM cusr_webpage "
		<< " JOIN cuser ON cusr_webpage.usr_id = cuser._id "
		<< " JOIN cwebpage ON cusr_webpage.wp_id = cwebpage._id "
		<< " WHERE cuser.login = '" << login << "'"
		<< " AND cwebpage.pname = '" << name << "';";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			qs.str(),
			obj_list{
			dbtext(), // name
			dbi64(), // date
			dbtext() // content
			}, true);
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		auto res = q->result();
		if (res.size() > 0) {
			auto r = res[0];
			auto p = WebPage(
					DBTEX(r, 0), // name
					std::chrono::system_clock::from_time_t(DBI64(r, 1)), // cdate
					DBTEX(r, 2) // content
					);
			_delegate->get_webpage_completed(n, p);
		} else
			_delegate->get_webpage_unk_completed(n);
	};
	return n;
}
