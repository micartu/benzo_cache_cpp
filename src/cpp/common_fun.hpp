#pragma once

#include <cstdint>

uint32_t get_patch_id(double lon, double lat);
