#pragma once

#include "blcache.hpp"
#include <memory>
#include <string>

namespace benzo_cache
{
	class BListenerCache;
	class Bcache;

	class User;
	class Transaction;
	class WebPage;
	class MapPatch;
	class MapPatchStations;
	class Station;

	class BlCacheImpl : public Blcache
	{
		public:
			// Constructor
			BlCacheImpl(const std::string& path);

			// general methods
			// user
			virtual bool exists_user(const std::string& login);
			virtual void create_user(const User& u);
			virtual std::optional<User> get_user(const std::string & login);
			virtual void update_user(const User& u);
			virtual void delete_user(const std::string& login);

			// transaction
			virtual bool exists_transaction(const std::string& login, const std::string& rrn);
			virtual void create_transaction(const std::string& login, const Transaction& t);
			virtual std::optional<Transaction> get_transaction(const std::string & login,
				   	const std::string& rrn);
			virtual void update_transaction(const Transaction& t);
			virtual void delete_transaction(const std::string& rrn);

			// webpage
			virtual bool exists_webpage(const std::string& login, const std::string& name);
			virtual void create_webpage(const std::string& login, const WebPage& p);
			virtual std::optional<WebPage> get_webpage(const std::string & login,
				   	const std::string& name);
			virtual void update_webpage(const std::string& login, const WebPage& p);
			virtual void delete_webpage(const std::string & login, const std::string& name);

			// patch
			virtual bool exists_patch(int32_t id);
			virtual void create_patch(const MapPatch& p);
			virtual std::optional<MapPatch> get_patch(int32_t id);
			virtual void update_patch(const MapPatch& p);
			virtual void delete_patch(int32_t id);

			// station
			virtual void create_stations(const std::vector<Station>& ss);
			virtual std::optional<MapPatchStations> get_patch_with_stations(int32_t id);
		private:
			std::shared_ptr<BListenerCache> _delegate;
			std::shared_ptr<Bcache> _cache;
	};

} // benzo_cache
