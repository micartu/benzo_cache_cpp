#include "bcacheimp.hpp"
#include <sstream>

using namespace benzo_cache;

seqnum BCacheImpl::exists_webpage(const std::string& login, const std::string& name)
{
	std::ostringstream qs;
	qs << "SELECT COUNT(*) FROM cusr_webpage "
		<< " JOIN cuser ON cusr_webpage.usr_id = cuser._id "
		<< " JOIN cwebpage ON cusr_webpage.wp_id = cwebpage._id "
		<< " WHERE cuser.login = '" << login << "'"
		<< " AND cwebpage.pname = '" << name << "';";
	return exists_common(qs.str(), [this](seqnum n, bool exists) {
		_delegate->webpage_exists_completed(n, exists);
	});
}
