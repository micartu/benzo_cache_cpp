#pragma once

#include "dobject.hpp"
#include <string>

namespace benzo_cache {

class dobject::data
{
public:
	// clones data
    virtual data* clone() const = 0;

	/// gives the name of the class
    virtual const char* class_name() const = 0;

	/// debug info about dobject
	virtual std::string to_string() const = 0;

	/// tries to make an object out of it string representation
	/// works for concrete class data
	virtual data* from_string(const std::string str) const = 0;

	virtual ~data() {};
};

} // benzo_cache
