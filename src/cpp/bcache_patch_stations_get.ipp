#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "map_patch.hpp"
#include "map_patch_stations.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::get_patch_with_stations(int32_t id)
{
	std::ostringstream qs;
	qs << "SELECT " << "station_id, " << fields_station << " FROM cpatch_station "
		<< " JOIN cstation on cpatch_station.station_id = cstation.id "
		<< " WHERE cpatch_station.patch_id = "
		<< (uint32_t)id << ";";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			qs.str(),
			obj_list{
				dbi32(), // id
				dbtext(), // name
				dbtext(), // descr
				dbtext(), // address
				dbi16(), // in_car_buy_fuel
				dbtext(), // img_small
				dbtext(), // img_big
				dbdouble(), // lon
				dbdouble(), // lat
				dbi16() // column_count
			}, true);
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n, id] (pquery q) {
		auto sts = q->result();
		if (sts.size() > 0) {
			std::vector<Station> ss;
			ss.reserve(sts.size());
			for (auto i = sts.begin(); i < sts.end(); ++i) {
				auto r = *i;
				auto s = Station(
					DBI32(r, 0), // id
					DBTEX(r, 1), // name
					DBTEX(r, 2), // desr
					DBTEX(r, 3), // address
					DBI16(r, 4) > 0 ? true : false, // in_car_buy_fuel
					DBTEX(r, 5), // img_small
					DBTEX(r, 6), // img_big
					DBDBL(r, 7), // lon
					DBDBL(r, 8), // lat
					DBI16(r, 9)  // column_count
					);
				ss.push_back(std::move(s));
			}
			auto p = MapPatchStations(id, ss);
			_delegate->get_patch_stations_completed(n, p);
		} else
			_delegate->get_patch_unk_completed(n);
	};
	return n;
}
