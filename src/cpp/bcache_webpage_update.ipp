#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "web_page.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::update_webpage(const std::string& login, const WebPage& p)
{
	std::ostringstream qs;
	qs << "UPDATE cwebpage SET "
		<< "cdate = " << std::chrono::system_clock::to_time_t(p.cdate) << ","
		<< "content = '" << p.content << "'"
		<< " WHERE _id = "
		<< "(SELECT cwebpage._id FROM cusr_webpage "
		<< " JOIN cuser ON cusr_webpage.usr_id = cuser._id "
		<< " JOIN cwebpage ON cusr_webpage.wp_id = cwebpage._id "
		<< " WHERE cuser.login = '" << login << "'"
		<< " AND cwebpage.pname = '" << p.name << "');";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			qs.str());
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->update_webpage_completed(n);
	};
	return n;
}
