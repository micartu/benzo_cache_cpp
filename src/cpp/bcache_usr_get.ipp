#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "user.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::get_user(const std::string& login)
{
	std::ostringstream qs;
	qs << "SELECT " << fields_user << " FROM cuser WHERE login = '"
		<< login << "';";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			qs.str(),
			obj_list{
			dbtext(), // login
			dbtext(), // name
			dbtext(), // phone
			dbtext(), // surname
			dbtext(), // midname
			dbtext(), // pin
			dbtext(), // card
			dbtext(), // email
			dbtext(), // codeword
			dbtext(), // fav_fuel
			dbi32(), // balance
			dbi64(), // menu_refresh_date
			dbi16(), // card_blocked
			// loyality
			dbtext(), // cpa_crd_code
			dbi32(), // cpa_crd_rub
			dbtext(), // cpa_prt_code
			dbtext(), // cpa_prt_name_
			dbtext(), // cpa_prt_ref1
			dbtext(), // cpa_prt_ref2
			dbtext(), // cpa_prt_ref3
			dbtext() // cpa_prt_subid
			}, true);
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		auto res = q->result();
		if (res.size() > 0) {
			auto r = res[0];
			auto l = UserLoyality(
					DBTEX(r, 13), // cpa_crd_code_
					DBI32(r, 14), // cpa_crd_rub_
					DBTEX(r, 15), // cpa_prt_code_
					DBTEX(r, 16), // cpa_prt_name_
					DBTEX(r, 17), // cpa_prt_ref1
					DBTEX(r, 18), // cpa_prt_ref2
					DBTEX(r, 19), // cpa_prt_ref3
					DBTEX(r, 20)  // cpa_prt_subid
					);
			auto u = User(
					DBTEX(r, 0), // login
					DBTEX(r, 1), // name
					DBTEX(r, 2), // phone
					DBTEX(r, 3), // surname
					DBTEX(r, 4), // midname
					"", // avatar
					DBTEX(r, 5), // pin
					DBTEX(r, 6), // card
					0, // birthday
					DBTEX(r, 7), // email
					DBTEX(r, 8), // codeword
					DBTEX(r, 9), // fav_fuel
					DBI32(r, 10), // balance
					DBI64(r, 11),  // menu_refresh_date
					l,
					DBI16(r, 12) > 0 ? true : false, // card_blocked
					"" // apn
					);
			_delegate->get_user_completed(n, u);
		} else
			_delegate->get_user_unk_completed(n);
	};
	return n;
}
