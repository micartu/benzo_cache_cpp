#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"
#include "common_dtypes.hpp"
#include "common_macro.hpp"

// entities
#include "user.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::update_user(const User& u)
{
	std::ostringstream qs;
	qs << "UPDATE cuser SET "
		<< "name = '" << u.name << "'" << ","
		<< "phone_number = '" << u.phone << "'" << ","
		<< "lastname = '" << u.surname << "'" << ","
		<< "middle_name = '" << u.midname << "'" << ","
		<< "crd_pin = '" << u.pin << "'" << ","
		<< "card = '" << u.card << "'" << ","
		<< "email = '" << u.email << "'" << ","
		<< "codeword = '" << u.codeword << "'" << ","
		<< "fav_fuel = '" << u.fav_fuel << "'" << ","
		<< "balance = " << u.balance << ","
		<< "menu_refresh_date = " << u.menu_refresh_date << ","
		<< "card_blocked = " << (u.card_blocked ? 1 : 0) << ","
		// loyality
		<< "cpa_crd_code = '" << u.loyality.cpa_crd_code << "'" << ","
		<< "cpa_crd_rub = " << (uint32_t)u.loyality.cpa_crd_rub << ","
		<< "cpa_prt_code = '" << u.loyality.cpa_prt_code << "'" << ","
		<< "cpa_prt_name = '" << u.loyality.cpa_prt_name << "'" << ","
		<< "cpa_prt_ref1 = '" << u.loyality.cpa_prt_ref1 << "'" << ","
		<< "cpa_prt_ref2 = '" << u.loyality.cpa_prt_ref2 << "'" << ","
		<< "cpa_prt_ref3 = '" << u.loyality.cpa_prt_ref3 << "'" << ","
		<< "cpa_prt_subid = '" << u.loyality.cpa_prt_subid << "'"
		<< " WHERE login = '" << u.login << "'"
		<< ";";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db, qs.str());
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->update_user_completed(n);
	};
	return n;
}
