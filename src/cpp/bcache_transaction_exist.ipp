#include "bcacheimp.hpp"
#include <sstream>

using namespace benzo_cache;

seqnum BCacheImpl::exists_transaction(const std::string& login, const std::string& rrn)
{
	std::ostringstream qs;
	qs << "SELECT COUNT(*) FROM cusr_transaction "
		<< " JOIN cuser ON cusr_transaction.usr_id = cuser._id "
		<< " JOIN ctransaction ON cusr_transaction.tra_id = ctransaction._id "
		<< " WHERE cuser.login = '" << login << "'"
		<< " AND ctransaction.rrn = '" << rrn << "';";
	return exists_common(qs.str(), [this](seqnum n, bool exists) {
		_delegate->transaction_exists_completed(n, exists);
	});
}
