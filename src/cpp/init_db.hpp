#pragma once

namespace benzo_cache
{
	const char* initialize_db =
		// users in system
		"CREATE TABLE IF NOT EXISTS cuser ("
		"   _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
		"   name TEXT,"
		"   middle_name TEXT,"
		"   lastname TEXT,"
		"   login TEXT,"
		"   balance INTEGER,"
		"   birthday INTEGER,"
		"   card TEXT,"
		"   card_blocked INTEGER,"
		"   codeword TEXT,"
		"   crd_pin TEXT,"
		"   email TEXT,"
		"   fav_fuel TEXT,"
		"   menu_refresh_date INTEGER,"
		"   phone_number TEXT,"
		// loyality part
		"   cpa_crd_code TEXT,"
		"   cpa_crd_rub INTEGER,"
		"   cpa_prt_code TEXT,"
		"   cpa_prt_name TEXT,"
		"   cpa_prt_ref1 TEXT,"
		"   cpa_prt_ref2 TEXT,"
		"   cpa_prt_ref3 TEXT,"
		"   cpa_prt_subid TEXT,"
		"   UNIQUE(_id, login)"
		");"

		// index for cuser.login
		"CREATE INDEX IF NOT EXISTS usr_login_index ON cuser(login);"

		// transactions of users in system
		"CREATE TABLE IF NOT EXISTS ctransaction ("
		"   _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
		"   rrn TEXT,"
		"   date INTEGER,"
		"   trn_type TEXT,"
		"   brand_name TEXT,"
		"   location TEXT,"
		"   product_name TEXT,"
		"   product_quantity REAL,"
		"   trn_bb INTEGER,"
		"   trn_value INTEGER,"
		"   points INTEGER,"
		"   status TEXT,"
		"   trn_comment TEXT,"
		"   convert_bonus_date INTEGER,"
		"   discount_sign INTEGER,"
		"   discount_value INTEGER,"
		"   discount_value_currency INTEGER,"
		"   channel TEXT,"
		"   inner_type TEXT,"
		"   UNIQUE(_id, rrn)"
		");"

		"CREATE TABLE IF NOT EXISTS cusr_transaction ("
		"   usr_id INTEGER,"
		"   tra_id INTEGER,"
		"   CONSTRAINT fk_user "
    	"      FOREIGN KEY (usr_id)"
    	"      REFERENCES cuser(_id)"
		"   CONSTRAINT fk_transaction "
    	"      FOREIGN KEY (tra_id)"
    	"      REFERENCES ctransaction(_id)"
		");"

		// webpages of users in system
		"CREATE TABLE IF NOT EXISTS cwebpage ("
		"   _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
		"   pname TEXT,"
		"   cdate INTEGER,"
		"   content BLOB,"
		"   UNIQUE(_id, pname)"
		");"

		"CREATE TABLE IF NOT EXISTS cusr_webpage ("
		"   usr_id INTEGER,"
		"   wp_id INTEGER,"
		"   CONSTRAINT fk_user "
		"      FOREIGN KEY (usr_id)"
		"      REFERENCES cuser(_id)"
		"   CONSTRAINT fk_webpage"
		"      FOREIGN KEY (wp_id)"
		"      REFERENCES cwebpage(_id)"
		");"

		// map_patch
		"CREATE TABLE IF NOT EXISTS cmap_patch ("
		"   id INTEGER,"
		"   cdate INTEGER"
		");"

		// index for cuser.login
		"CREATE INDEX IF NOT EXISTS map_patch_id ON cmap_patch(id);"

		// station
		"CREATE TABLE IF NOT EXISTS cstation ("
		"   id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
		"   lon REAL,"
		"   lat REAL,"
		"   sname TEXT,"
		"   descr TEXT,"
		"   addr TEXT,"
		"   in_car_buy_fuel INTEGER,"
		"   img_small TEXT,"
		"   img_big TEXT,"
		"   column_count INTEGER"
		");"

		// index for cstation.(lon,lat)
		"CREATE INDEX IF NOT EXISTS station_lon_lat ON cstation(lon,lat);"

		"CREATE TABLE IF NOT EXISTS cpatch_station ("
		"   patch_id INTEGER,"
		"   station_id INTEGER,"
		"   CONSTRAINT fk_patch "
		"      FOREIGN KEY (patch_id)"
		"      REFERENCES cmap_patch(id)"
		"   CONSTRAINT fk_station"
		"      FOREIGN KEY (station_id)"
		"      REFERENCES cstation(id)"
		");"

		// migration control of all tables in db
		"create table if not exists cversion ("
		"   _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
		"   name TEXT,"
		"   UNIQUE(_id, name)"
		");"

		"";
} // benzo_cache
