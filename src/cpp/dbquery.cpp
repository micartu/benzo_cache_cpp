#include "dbquery.hpp"
#include "common_dtypes.hpp"
#include <functional>
#include <string>

using namespace benzo_cache;

static const std::string row_id = "%row_id";
static const std::string ds = "%s";

static std::vector<std::string> codewords = {
	ds,
	row_id,
};

/// finds in given string parameters in form (xx,yy,...)
/// and returns them in vector of integers
static const std::vector<int> parse_params(const std::string& str)
{
	std::vector<int> out;
	const auto begin = str.find('(');
	if (begin != std::string::npos) {
		const auto end = str.find(')', begin + 1);
		if (end != std::string::npos) {
			auto fbegin = begin + 1;
			auto fpos = begin;
			bool endpos = false;
			while (!endpos) {
				if (!endpos && (fpos = str.find(',', fpos + 1)) == std::string::npos) {
					endpos = true;
					fpos = end;
				}
				if (fpos > fbegin) {
					auto digit = str.substr(fbegin, fpos - fbegin);
					int d = std::stoi(digit);
					out.push_back(d);
					fbegin = fpos + 1;
				}
			}
		}
	}
	return out;
}

// manual type queries-constructor
dbquery::dbquery(sqlite3pp::database& db,
		std::vector<std::string>&& queries,
		std::vector<bool>&& fetches,
		std::vector<vector_objs>&& params)
	: _db(db),
	_queries(std::move(queries)),
	_fetches(std::move(fetches)),
	_result(),
	_params(std::move(params))
{
}

// execute only queries-constructor
dbquery::dbquery(sqlite3pp::database& db,
		std::vector<std::string>&& queries)
	: _db(db),
	_queries(std::move(queries)),
	_fetches(_queries.size(), false),
	_result(),
	_params(_queries.size(), vector_objs{})
{
}

dbquery::dbquery(sqlite3pp::database& db,
		std::initializer_list<std::string> queries,
		std::initializer_list<bool> fetches,
		std::initializer_list<std::initializer_list<dobject>> args)
	: _db(db),
	_queries(queries),
	_fetches(fetches),
	_result(),
	_params()
{
	for (auto al : args) {
		vector_objs objs{al};
		_params.push_back(std::move(objs));
	}
}

dbquery::dbquery(sqlite3pp::database& db,
		std::string query,
		std::initializer_list<dobject> objs,
		bool fetch)
	: _db(db),
	_queries{query},
	_fetches{fetch},
	_result(),
	_params{vector_objs{objs}} { }

dbquery::dbquery(sqlite3pp::database& db,
		std::string query,
		std::vector<dobject> params)
	: _db(db),
	_queries{query},
	_fetches{false},
	_result(),
	_params{params} { }

dbquery::~dbquery() { }

void dbquery::check_row_ids(std::vector<bool>& grab_rid)
{
	int r = 0;
	for (auto q = _queries.begin();
			q != _queries.end(); ++q, ++r) {
		auto fpos = (*q).find(row_id);
		if (fpos != std::string::npos) {
			auto end = (*q).find(')', fpos + 1);
			if (end != std::string::npos) {
				auto s = (*q).substr(fpos, end - fpos + 1);
				auto p = parse_params(s);
				if (p.size() > 0) {
					int i = p[0];
					if (i < grab_rid.size()) {
						grab_rid[i] = true;
					}
				}
			}
		}
	}
}

std::string dbquery::prepare_query(const std::string& query) const
{
	const std::string unk = "?";
	std::string q(query);
	for (auto cw : codewords) {
		std::string::size_type start_p = 0;
		std::string::size_type fp;
		// find given codeword in query:
		while ((fp = q.find(cw, start_p)) != std::string::npos) {
			std::string to("");
			std::string::size_type length = 0;
			// find the end of parameter clause:
			auto fe = q.find(')', fp + 1);
			if (fe != std::string::npos) {
				length = fe - fp + 1; // length of whole parameter in query
				// operation in given query with parameters:
				auto op = q.substr(fp, length);
				// extract parameters from current operation as integers:
				auto p = parse_params(op);
				// what kind of operation we're dealing with?
				if (op.rfind(row_id, 0) == 0) { // last inserted row?
					if (p.size() > 0 &&
							p[0] < _result.size() &&
							_result[p[0]].size() > 0) {
						auto r = _result[p[0]];
						dbi64 d = r[0][0];
						to = d.to_string();
					} else
						to = unk;
				} else if (op.rfind(ds, 0) == 0) { // string (int, double, etc.)?
					if (p.size() > 1 &&
							p[0] < _result.size() &&
							_result[p[0]].size() > 0 &&
							p[1] < _result[p[0]][0].size()) {
						auto r = _result[p[0]];
						dobject t = r[0][p[1]];
						to = t.to_string();
					} else
						to = unk;
				}
			}
			q.replace(fp, length, to);
			start_p += fp + to.length();
		}
	}
	return q;
}

void dbquery::execute()
{
	assert(_fetches.size() == _queries.size());
	assert(_fetches.size() == _params.size());
	std::vector<bool> grab_rid(_fetches.size(), false);
	int f = 0;
	auto p = _params.begin();
	check_row_ids(grab_rid);
	for (auto q = _queries.begin();
			q != _queries.end(); ++q, ++f, ++p) {

		// try to replace codewords with real data:
		auto query = prepare_query(*q);

		std::vector<vector_objs> result;
		if (_fetches[f]) { // a fetch query
			sqlite3pp::query fq(_db, query.c_str());
			for (auto i = fq.begin(); i != fq.end(); ++i) {
				vector_objs objs;
				int j = 0;
				std::for_each((*p).begin(), (*p).end(),
						[&i, &j, &objs](dobject const& o) {
						std::string value((*i).get<char const*>(j));
						dobject cur(o, value);
						objs.push_back(std::move(cur));
						j++;
						});
				result.push_back(std::move(objs));
			}
			fq.finish();

		} else { // a simple execute statement

			sqlite3pp::command cmd(_db, query.c_str());
			int param = 1;
			std::for_each((*p).begin(), (*p).end(),
					[&cmd, &param](dobject const& o)
					{
					cmd.bind(param, o.to_string(), sqlite3pp::copy);
					param++;
					}
					);
			cmd.execute();
			// should we save the last inserted row-id?
			if (grab_rid[f]) {
				vector_objs objs;
				dbi64 r(_db.last_insert_rowid());
				objs.push_back(std::move(r));
				result.push_back(std::move(objs));
			}
		}
		_result.push_back(std::move(result));
	}
}
