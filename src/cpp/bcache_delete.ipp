// common file with list of other sections

// user deletion
#include "bcache_usr_delete.ipp"

// transaction
#include "bcache_trans_delete.ipp"

// webpage
#include "bcache_webpage_delete.ipp"

// patch
#include "bcache_patch_delete.ipp"
