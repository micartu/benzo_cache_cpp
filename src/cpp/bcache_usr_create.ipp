#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"

// entities
#include "user.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::create_user(const User& u)
{
	// almost generated part of query:
	std::ostringstream qs;
	//qs << "DELETE FROM cuser WHERE login = '" << u.login << "';"
	//	<< "INSERT INTO cuser (" <<	fields_user << ") VALUES ("
	qs 	<< "INSERT INTO cuser (" <<	fields_user << ") VALUES ("
		<< "'" << u.login << "'" << ","
		<< "'" << u.name << "'" << ","
		<< "'" << u.phone << "'" << ","
		<< "'" << u.surname << "'" << ","
		<< "'" << u.midname << "'" << ","
		<< "'" << u.pin << "'" << ","
		<< "'" << u.card << "'" << ","
		<< "'" << u.email << "'" << ","
		<< "'" << u.codeword << "'" << ","
		<< "'" << u.fav_fuel << "'" << ","
		<< u.balance << ","
		<< u.menu_refresh_date << ","
		<< (u.card_blocked ? 1 : 0) << ","
		// loyality
		<< "'" << u.loyality.cpa_crd_code << "'" << ","
		<< (uint32_t)u.loyality.cpa_crd_rub << ","
		<< "'" << u.loyality.cpa_prt_code << "'" << ","
		<< "'" << u.loyality.cpa_prt_name << "'" << ","
		<< "'" << u.loyality.cpa_prt_ref1 << "'" << ","
		<< "'" << u.loyality.cpa_prt_ref2 << "'" << ","
		<< "'" << u.loyality.cpa_prt_ref3 << "'" << ","
		<< "'" << u.loyality.cpa_prt_subid << "'"
		<< ");";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db, qs.str());
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->create_user_completed(n);
	};
	return n;
}
