#pragma once

#define DBTEX(result, i) \
	(static_cast<dbtext>(result[i])).get_text()

#define DBI16(result, i) \
	(static_cast<dbi16>(result[i])).get_val()

#define DBI32(result, i) \
	(static_cast<dbi32>(result[i])).get_val()

#define DBI64(result, i) \
	(static_cast<dbi64>(result[i])).get_val()

#define DBDBL(result, i) \
	(static_cast<dbdouble>(result[i])).get_val()
