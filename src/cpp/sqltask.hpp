#pragma once

#include "common_types.hpp"

namespace benzo_cache
{
	class sqltask
	{
		public:
			sqltask(pquery query, seqnum seq)
				: _query(query),
				_seq(seq) { }

			~sqltask() { }

			pquery getQuery() { return _query; }
			seqnum getSeq() { return _seq; }

		private:
			pquery _query;
			seqnum _seq;
	};

} // benzo_cache
