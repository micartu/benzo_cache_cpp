// common file with list of other sections

// get user
#include "bcache_usr_get.ipp"

// get transaction
#include "bcache_trans_get.ipp"

// get webpage
#include "bcache_webpage_get.ipp"

// get patch
#include "bcache_patch_get.ipp"
#include "bcache_patch_stations_get.ipp"
