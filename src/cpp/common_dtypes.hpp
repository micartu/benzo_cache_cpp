#pragma once

#include "dbvalue.hpp"
#include "dbtext.hpp"

namespace benzo_cache
{
	using dbi16 = dbvalue<int16_t>;
	using dbi32 = dbvalue<int32_t>;
	using dbi64 = dbvalue<int64_t>;
	using dbdouble = dbvalue<double>;
	using dbinteger = dbvalue<int>;
}
