#include "bcacheimp.hpp"
#include <sstream>

using namespace benzo_cache;

seqnum BCacheImpl::delete_patch(int32_t id)
{
	std::vector<std::string> queries;
	std::ostringstream qs;
	qs << "DELETE FROM cmap_patch WHERE id = " << (uint32_t)id << ";";
	queries.push_back(qs.str());
	qs.str("");
	qs << "DELETE FROM cstation WHERE id = ("
		<< "SELECT station_id FROM cpatch_station WHERE patch_id = " << (uint32_t)id << ");";
	queries.push_back(qs.str());
	qs.str("");
	qs << "DELETE FROM cpatch_station WHERE patch_id = " << (uint32_t)id << ";";
	queries.push_back(qs.str());
	auto q = std::make_shared<benzo_cache::dbquery>(*_db, std::move(queries));
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->delete_patch_completed(n);
	};
	return n;
}
