#include "bcacheimp.hpp"
#include <sstream>
#include "query_strs.hpp"

// entities
#include "web_page.hpp"

using namespace benzo_cache;

seqnum BCacheImpl::create_webpage(const std::string& login, const WebPage& p)
{
	// transaction creation query:
	std::ostringstream wp_qs;
	wp_qs << "INSERT INTO cwebpage (" << fields_webpage << ") VALUES ("
		<< "'" << p.name << "'" << ","
		<< std::chrono::system_clock::to_time_t(p.cdate) << ","
		<< "'" << p.content << "'"
		<< ");";
	// find out user's id query:
	std::ostringstream usr_id_qs;
	usr_id_qs << "SELECT _id FROM cuser WHERE login = "
		<< "'" << login << "'" << ";";
	// and insert that data into aggregate table cusr_webpage:
	const std::string usr_webpage_qs =
		"INSERT INTO cusr_webpage (usr_id, wp_id) VALUES (%s(1,0), %row_id(0));";
	auto q = std::make_shared<benzo_cache::dbquery>(*_db,
			std::initializer_list<std::string> {
				wp_qs.str(),
				usr_id_qs.str(),
				usr_webpage_qs
			},
			std::initializer_list<bool> {
				false, // no fetch
				true, // fetch
				false // no fetch
			},
			std::initializer_list<obj_list> {
				{}, // nothing here (would be only row_id)
				{
				dbtext(), // id
				},
				{}, // nothing, only INSERT-operation
			});
	auto n = _exe->put(q);
	std::lock_guard<std::mutex> guard(_mutex);
	_ops[n] = [this, n] (pquery q) {
		_delegate->create_webpage_completed(n);
	};
	return n;
}
