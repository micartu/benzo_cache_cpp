Pod::Spec.new do |s|
	s.ios.deployment_target = '9.3'
	s.name             = 'benzo_cache_sqlite_cpp_lib'
	s.version          = '1.0.0'
	s.summary          = 'benzo cache support library (c++)'
	s.requires_arc 	   = true

	s.description      = <<-DESC
Djinni Cache Support Library for benzo (C++ <-> objective C bridge)
	DESC

	s.homepage         = 'https://github.com/micartu/djinni_support_benzo_lib'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'micartu' => 'michael.artuerhof@gmail.com' }
	s.source           = { :git => 'https://github.com/micartu/djinni_support_benzo_lib.git', :tag => s.version.to_s }

	# generated djinni files + monolith sqlite3 + needed libraries => all in one BIG library
	s.source_files = 'generated-src/cpp/**/*.{cpp,hpp}',
 				'generated-src/objc/**/*.{mm,h}',
 				'deps/djinni/support-lib/*.{hpp}',
 				'deps/djinni/support-lib/objc/**/*.{mm,h}',
				'src/cpp/**/*.{cpp,ipp,hpp,h}',
 				'deps/sqlite3pp/headeronly_src/**/*.{h,ipp}',
 				'deps/sqlite3/*.{h}',
 				'deps/sqlite3/sqlite3.c'
	s.private_header_files = 'deps/djinni/support-lib/objc/*.{h,hpp}',
							'generated-src/cpp/*.hpp'
	s.public_header_files = 'generated-src/objc/*.h'
	s.exclude_files = 'generated-src/objc/*+Private.h'
	s.libraries = 'stdc++'
	s.xcconfig = {
		'USER_HEADER_SEARCH_PATHS' => '"${PROJECT_DIR}/../deps/benzo_cache_cpp/deps/djinni/support-lib/objc" "${PROJECT_DIR}/../deps/benzo_cache_cpp/generated-src/cpp"',
		'CLANG_CXX_LANGUAGE_STANDARD' => 'c++17',
		'CLANG_CXX_LIBRARY' => 'libc++'
	}
	s.frameworks = 'Foundation'
end
